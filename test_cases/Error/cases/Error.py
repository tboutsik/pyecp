# -*- coding: utf-8 -*-
"""
Created on Mon Oct 16 16:36:35 2017

Electrostatic Charged Particles test case

@author: a.boutsik
"""
import time
import os
import numpy as np
import subprocess
from pylab import *
import matplotlib.pyplot as plt

from pyecp.mesh  import *
from pyecp.particles import *
from pyecp.gconfig import *
from pyecp.pyecp import *
from pyecp.postprocess import *
from pyecp.inout import *
from pyecp.initialization import *

#------------------------------------------------------------------------------
#                  TEST CASE SETUP STARTS AFTER THIS LINE
#------------------------------------------------------------------------------

global ke, eps
ke = gconfig.ke
eps = gconfig.eps

#Particle properties
dispersion = 0                          #diameter dispersion 0: monodisperse, 1: polydisperse
vdistrib   = 'zero'                     #type of particle velocity distribution: random, uniform, zero, file
npart      = [80]                       #total number of particles
diameter   = 360*10**(-6)               #maximum diameter of particles
rho        = 910                        #density of the solid phase (particles) e.g. polyethylene
ncelld     = 3                          #cell size-to-diameter ratio
clearance  = 10**(-6)*diameter          #clearence between particles
icharge    = 10**(-14)                  #initial particles charge
cfl        = 1.                         #CFL-type condition for lagrangian tracking of particles <=2.5

#Computational domain
ldx = 50; ldy = 50; ldz = 50
xmin = -ldx*diameter ; xmax = ldx*diameter #x borders
ymin = -ldy*diameter ; ymax = ldy*diameter #y borders
zmin = -ldz*diameter ; zmax = ldz*diameter #z borders
borders = np.array([[xmin, xmax], [ymin, ymax], [zmin, zmax]])
epsilon = 0                       #distance for virtual particles domain measured in particle dimeters--> epsilon < ld
nf = [2*(i+1) for i in range(10)]  #number of cells in x, y, z direction for manual
#nf = [2*(i+1) for i in range(2)] #number of cells in x, y, z direction for manual

#Eulerian grid: eq_particle, poisson
#meshtype = 'eq_particle'
#meshtype = 'poisson'
meshtype = 'manual'

#boundary conditions
bcx = 0 #0: periodic, 1: wall
bcy = 0 #0: periodic, 1: wall
bcz = 0 #0: periodic, 1: wall
boundary = [bcx, bcy, bcz]

#electric force algorithm: direct, extended, optimized
eforce  = ['direct', 'extended', 'optimized']
eforce2 = [''] + (len(eforce)-1)*[' $p-m$']

sdistrib = ['file' for i in range(len(eforce)) ] #type of particle spatial distribution:  random, uniform, file
sdistrib[0] = 'random'

#artificial drag (viscous dissipation of kinetic energy)
dforce = 0 #artificial drag 0: no drag, 1: drag 

#Output options
it_step  = 2 #iteration step for which data is written in .txt files
pl_step  = 2 #iteration step for which data is plotted in .pdf files

savetxt  = 1 #option to save the txts                        0: no, 1: yes
drawplot = 0 #option to create the plots                     0: no, 1: yes
showplot = 1 #option to show the plot in the terminal        0: no, 1: yes (block=True) 2:yes (block=False)
saveplot = 1 #option to save the plots                       0: no, 1: yes
tracking = 0 #option to plot the trajectory of the particles 0: no, 1: yes
vectors  = 1 #option to plot the velocity and force vectors  0: no, 1: yes

def cost(i, Nf, N, per):

    Npc = float(N)/float(Nf**3)
    
    if eforce[i] == 'optimized':
        cost = N*(27*Npc-1 + per*Nf**3 - 27)
        
    elif eforce[i] == 'extended':
        cost = N*(27*Npc-1 + Nf**3 - 27)*per
    
    return cost

def optimal(i, N):

    per = (2*nper+1)**dper

    if eforce[i] == 'optimized':
        Nf = np.sqrt(3)*(float(N)/float(per))**(1./6.)
        
    elif eforce[i] == 'extended':
        Nf = np.sqrt(3)*N**(1./6.)

    nceil  = int(math.ceil(Nf))
    nfloor = int(math.floor(Nf))
    
    #cost of particle-mesh algorithm
    cceil  = cost(i, nceil,  N, per)   
    cfloor = cost(i, nfloor, N, per)
    
    if cceil < cfloor: #choosing the integer that minimizes the computational cost
        Nf = nceil
    else: #cceil <= cfloor
        Nf = nfloor
    
    #checking if number of cells is equal to 1 and imposing it equal to 2
    if Nf == 1:
        Nf = 2    
    
    return Nf

def simulation(i, j, k):
    
    print '***********************************************************************'
    print '                  ELECTRICALLY CHARGED PARTICLES                       '
    print '***********************************************************************'
    print

    Nf = nf[k]    
    gconfig.current_time = time.clock()
    
    #Setting particle properties
    myparticles = particles(dispersion, sdistrib[i], vdistrib, npart[j], diameter, clearance, rho, icharge, eforce[i], dforce, cfl, Nf)
    
    #Cartesian mesh creation
    mymesh = domain3D(meshtype, myparticles, borders, boundary, epsilon, ncelld, Nf)
    mymesh.cartesian_check()
    
    #Solving electrically charged particle mouvement up to tfinal or itfinal
    mypostprocess = postprocess(myparticles, mymesh)
    myinout = inout(myparticles, it_step, pl_step, savetxt, drawplot, saveplot, showplot, tracking, vectors)
    myinit = initialization(myparticles)
    mypyecp = pyecp(tfinal, itfinal)
    mypyecp.solve(mymesh, myparticles, mypostprocess, myinout, myinit)

    print '-------------------------------------------------------------------'
    print "Cpu time:" , mypyecp.t_total, "s"
    print "To solve the interaction of %d electrostatic particles for %2.5f seconds using %s algorithm" %(myparticles.npart, mypyecp.t, eforce[i])
    print 'Characteristic time of Coulomb interaction %2.5f' %tpe

    #simulation time of the electrostatic force calculation module per iteration
    simtime[i, j, k] = mypyecp.t_it[2]

    #for the direct algorithm only, initializations are saved in order be used as reference for the error calculation
    if i == 0:
        sit = myinout.it_to_sit(itfinal)  
        outdir = './particles/txt/'
        txtname = 'p_' + myinout.suffix + '_' + sit + '.txt'
        initname = 'init_' + str(myparticles.npart) + '_' + ("mono" if myparticles.dispersion == 0 else "poly") + '.txt'
        print 'cp '+ outdir + txtname + ' ../cases/' + initname
        subprocess.call(['cp', outdir + txtname, '../cases/' + initname])
    
        return sit
    
    else:
        #for the particle-mesh algorithms, return the number of cells in case it was changed during the simulation (to accomodate electro-periodicity)     
        return mymesh.nf

def relative_error(qcal, qref):
    "calculates the relative error between a calculated quantity q and its reference value qref"
    
    error = np.abs((qref - qcal)/qref)
    
    return error

def write_error(i, j, errorpercent, suffix, sit):
    
    outdir = './' + str(npart[j]) + '/'
    txtname = 'error_' + suffix + '_' + sit + '.txt'
    h = '\n'.join(['Percentage of particles in error zones','Cells: ' + str(nf)])
    zones  = ['<1%', '<5%', '<10%', '<20%', '<50%', '>50%']
    fmt = ['{%d:>6}' %k for k in range(1, len(nf)+1)]
    string = ' '
    for k in range(len(fmt)):
        string += fmt[k] + ' '
          
    args = [() for k in np.arange(len(errorpercent))]

    #construction of each line
    for k in np.arange(len(errorpercent)):
        stuple = ['%2.2f' %(errorpercent[k,l]) for l in range(len(nf)) ] #list of all the formatted numbers to be written per line
        stuple.insert(0, '%5s' %(zones[k]))                              #insert the error zones at the top of the list
        args[k] = tuple(stuple)                                          #convert list to tuple

    print
    #writing results in output file    
    with open(outdir+txtname, 'w') as output:
        output.write(h+'\n') 
        for k in np.arange(len(errorpercent)):
            output.write(str('{0:<4}' + string + '\n').format(*args[k]))
            #printing on the screen
            if k == 0: print 'Error distribution for %d particles using the ' %(npart[j]) + eforce[i] + ' algorithm' 
            print str('{0:<4}' + string).format(*args[k])
            
    print

def plot_error(i, j, errorpercent, suffix, sit):
    
    #plotting figures for each number of particles and per algorithm
    fig = plt.figure()
    ax1 = fig.add_subplot(111)
    algo = [k+str(n) for k,n in zip(eforce,eforce2)]

    #colours and labels for the 6 error zones
    colors  = ['darkgreen', 'lightgreen', 'yellow', 'red', 'darkred', 'k']
    labels  = [r'$<1\%$', r'$<5\%$', r'$<10\%$', r'$<20\%$', r'$<50\%$', r'$>50\%$']
    
    shape = tuple(map(sum, zip(errorpercent.shape, (1,0)))) #increasing the shape by one row in order to convert error zones (N) to error levels (N+1) 
    percentage = np.zeros(shape)                            #matrix whose rows span the error levels (7) and colums span the number of Nf values
    
    #accumulation of particle percentages
    #initialization of matrices
    sumpercent = np.zeros(len(nf))
    percentage[0] = sumpercent
    for l in np.arange(1,len(percentage)):
        sumpercent += errorpercent[l-1]
        percentage[l] = sumpercent
        plt.plot(nf, percentage[l], marker = 'o', color= 'k', linestyle = '-', markersize = 2) #simulation time data
    
    #filling in between the 6 error zones
    for l in np.arange(len(percentage)-1):
        plt.fill_between(nf, percentage[l], percentage[l+1], color = colors[l], label = labels[l], alpha = 0.8) 
    
    plt.xlabel(r'Number of cells $N_f$')
    plt.title(r'Error of the force magnitude using ' + algo[i] + r' algorithm',fontsize=20)
    plt.xticks(nf)

    #configuring ax1
    ax1.set_ylabel(r'Percentage of Particles (%d total)' %npart[j])
    plt.grid()
    
    if min(percentage[1]) != 0: 
        miny = min(percentage[1])-1.
    else:
        miny = min(percentage[1])
    ax1.set_ylim(miny, 100.)

    ax1.legend(loc='lower right')

    #configuring ax2
    ax2 = ax1.twinx()
    ax2.plot(nf, simtime[i, j, :], 'b-', lw = 2)
    ax2.set_ylabel(r'Simulation time (1 it) $T_s \ (s)$', color='b')
    for tl in ax2.get_yticklabels():
        tl.set_color('b')

    #add a horizontal line where the direct algorithm cost is    
    x1, x2 = min(nf), max(nf)
    y1 = y2 = simtime[0,j,0]
    ax2.plot((x1, x2), (y1, y2), c = 'w', ls = '-.',  lw = 2)

    #add a vertical line where is the optimal number of cells
    k = nf.index(Nfopt[i,j])

    #add a horizontal line where the optimal cost is    
    x1, x2 = Nfopt[i,j], max(nf)
    y1 = y2 = simtime[i,j,k]
    ax2.plot((x1, x2), (y1, y2), c = 'b', ls = '--',  lw = 1)

    #add a vertical line where the optimal number of cells is
    x1 = x2 = Nfopt[i,j]
    y1, y2 = ax2.get_yticks()[0], simtime[i,j,k]
    ax2.plot((x1, x2), (y1, y2), c = 'b', ls = '--',  lw = 1)

#    plt.show()
    #saving the plot
    outdir = './' + str(npart[j]) + '/'
    pdfname = 'error_' + suffix + '_' + sit + '.pdf'
    pp = PdfPages(outdir+pdfname)
    pp.savefig(fig, bbox_inches=None)
    pp.close()
    plt.close(fig)
    
#------------------------------------------------------------------------------
#        ERROR TEST CASE FOR 2 ALGORITHMS USING DIRECT AS REFERENCE           #
#------------------------------------------------------------------------------

#calculate several mesh attributes that will be needed later (mymesh is not available at this point)
rc = np.sqrt(ke/eps)*icharge
lx = xmax - xmin
nper = int(np.ceil(rc/lx)) - 1  #number of domains to span to cover rc
dper = 0                        #number of periodic dimensions
for i in range(3):
    if boundary[i] == 0:
        dper += 1

Nfopt = np.zeros((len(eforce), len(npart)), dtype = int)

for i in np.arange(len(eforce)):
    if i!=0:
        for j in np.arange(len(npart)):
#            print 'algo ', eforce[i], ' and ', npart[j], ' particles' 
            Nfopt[i,j] = optimal(i, npart[j])
            if Nfopt[i,j] not in nf:
                nf.append(Nfopt[i,j])
nf.sort()

simtime = np.zeros((len(eforce), len(npart), len(nf)))

#Simulation loop
for i in np.arange(len(eforce)):
        
        for j in np.arange(len(npart)):
            
            mp = 4./3.*rho*np.pi*(diameter/2.)**3
            le = max((xmax-xmin),(ymax-ymin),(zmax-zmin))            #characteristic length of Coulomb's interaction    
            tpe = np.sqrt(mp*le**3/(npart[j]*gconfig.ke*icharge**2)) #characteristic time scale of Coulomb's interaction
            
            #Time and iterations configuration
            tfinal   = 8*tpe #final time
            itfinal  = 0  #final iteration
            
            if i!=0:
                for k in np.arange(len(nf)):
                    #in case number of cells is changed during the simulation (to accomodate electro-periodicity)
                    nf[k] = simulation(i, j, k)
            else:
                sit = simulation(i, j, 0)

#calculation of error per number of particles and for different number of cells (for iteration itfinal)
indir = './particles/txt/'

fref       = [ [] for k in range(len(npart)) ]                                                                 #force reference
fcal       = [ [ [ [] for k in  range(len(nf))  ] for j in range(len(npart)) ] for i in range(1,len(eforce)) ] #force calculated for extended and optimized
ferror     = [ [ [ [] for k in  range(len(nf))  ] for j in range(len(npart)) ] for i in range(1,len(eforce)) ] #force error for extended and optimized
errorclass = [ [ [ [] for k in  range(len(nf))  ] for j in range(len(npart)) ] for i in range(1,len(eforce)) ] #particle error classes for extended and optimized

#Error calculation loop over the two algorithms excluding direct

for i in np.arange(len(eforce)):
        
    for j in np.arange(len(npart)):
        
        #We get the reference solution from the direct simulation results
        if i == 0:

            txtdirect = 'p_' + str(npart[j]) + '_' + sdistrib[0] + '_' + ("mono" if dispersion == 0 else "poly") + '_' + eforce[0] + '_' + sit + '.txt'
            fref[j] = np.genfromtxt(indir+txtdirect, usecols = (8), skip_header=2, unpack=True)
            
            #creating directories for error plots per number of particles
            directory = './' + str(npart[j])
            if not os.path.exists(directory):
                os.makedirs(directory)
        
        #for the particle-mesh algorithms we perform the error calculation
        else:
        
            readsuffix  = str(npart[j]) + '_' + sdistrib[i] + '_' + ("mono" if dispersion == 0 else "poly") + '_' + eforce[i]
            writesuffix = str(npart[j]) + '_' + ("mono" if dispersion == 0 else "poly") + '_' + eforce[i]
    
            for k in np.arange(len(nf)):
                
                txteforce = 'p_' + readsuffix + '_' + str(nf[k]) + '_' + sit + '.txt'            
                fcal[i-1][j][k]   = np.genfromtxt(indir+txteforce, usecols = (8), skip_header=2, unpack=True)
                ferror[i-1][j][k] = relative_error(fcal[i-1][j][k], fref[j])
                #calculate the percentage of particles with different zones of error (<1%, <5%, <10%, <20%, <50%, >50%)
                errorclass[i-1][j][k] = np.zeros(6)         #6 error zones
                for p in np.arange(npart[j]):
                    if ferror[i-1][j][k][p] < 0.01: 
                        errorclass[i-1][j][k][0] += 1                  
                    elif ferror[i-1][j][k][p] < 0.05:
                        errorclass[i-1][j][k][1] += 1
                    elif ferror[i-1][j][k][p] < 0.10:
                        errorclass[i-1][j][k][2] += 1
                    elif ferror[i-1][j][k][p] < 0.20:
                        errorclass[i-1][j][k][3] += 1
                    elif ferror[i-1][j][k][p] < 0.50:
                        errorclass[i-1][j][k][4] += 1
                    else: 
                        errorclass[i-1][j][k][5] += 1
                
                errorclass[i-1][j][k] = errorclass[i-1][j][k]*100./npart[j]
                
            #writing and plotting percentages of particles along number of cells
            errorpercent = np.array(errorclass[i-1][j]).transpose()   #matrix whose rows span the error zones (6) and columns span the number of Nf values
            write_error(i, j, errorpercent, writesuffix, sit)
            plot_error(i, j, errorpercent, writesuffix, sit)