# -*- coding: utf-8 -*-
"""
Created on Mon Oct 16 16:36:35 2017

Electrostatic Charged Particles test case

@author: a.boutsik
"""
import time
import numpy as np
import subprocess
from pylab import *
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
matplotlib.rcParams['text.usetex'] = True
matplotlib.rcParams['font.family'] = 'serif'

from pyecp.mesh  import *
from pyecp.particles import *
from pyecp.gconfig import *
from pyecp.pyecp import *
from pyecp.postprocess import *
from pyecp.inout import *
from pyecp.initialization import *

#------------------------------------------------------------------------------
#                  TEST CASE SETUP STARTS AFTER THIS LINE
#------------------------------------------------------------------------------

global ke, eps
ke = gconfig.ke
eps = gconfig.eps

#Particle properties
dispersion = 0                               #diameter dispersion 0: monodisperse, 1: polydisperse
sdistrib   = ['random', 'file', 'file']      #type of particle spatial distribution:  random, uniform, file
vdistrib   = 'zero'                          #type of particle velocity distribution: random, uniform, zero, file
npart      = [10 * 2**i for i in range(10)]  #total number of particles
diameter   = 360*10**(-6)                    #maximum diameter of particles
rho        = 910                             #density of the solid phase (particles) e.g. polyethylene
ncelld     = 3                               #cell size-to-diameter ratio
clearance  = 10**(-6)*diameter               #clearence between particles
icharge    = 10**(-14)                       #initial particles charge
cfl        = 1.                              #CFL-type condition for lagrangian tracking of particles <=2.5

#Computational domain
ldx = 50; ldy = 50; ldz = 50
xmin = -ldx*diameter ; xmax = ldx*diameter #x borders
ymin = -ldy*diameter ; ymax = ldy*diameter #y borders
zmin = -ldz*diameter ; zmax = ldz*diameter #z borders
borders = np.array([[xmin, xmax], [ymin, ymax], [zmin, zmax]])
epsilon = 0 #distance for virtual particles domain measured in particle dimeters--> epsilon < ld/2
nf = nan    #number of cells in x, y, z direction for manual

#Eulerian grid: eq_particle, poisson
meshtype = 'eq_particle'
#meshtype = 'poisson'
#meshtype = 'manual'

#boundary conditions
bcx = 0 #0: periodic, 1: wall
bcy = 0 #0: periodic, 1: wall
bcz = 0 #0: periodic, 1: wall
boundary = [bcx, bcy, bcz]

#electric force algorithm: direct, extended, optimized
eforce = ['direct', 'optimized', 'extended']
eforce2 = [''] + (len(eforce)-1)*[' $p-m$']

#artificial drag (viscous dissipation of kinetic energy)
dforce = 0 #artificial drag 0: no drag, 1: drag 

#Output options
it_step  = 2 #iteration step for which data is written in .txt files
pl_step  = 2 #iteration step for which data is plotted in .pdf files

savetxt  = 1 #option to save the txts                        0: no, 1: yes
drawplot = 0 #option to create the plots                     0: no, 1: yes
showplot = 0 #option to show the plot in the terminal        0: no, 1: yes (block=True) 2:yes (block=False)
saveplot = 1 #option to save the plots                       0: no, 1: yes
tracking = 0 #option to plot the trajectory of the particles 0: no, 1: yes
vectors  = 1 #option to plot the velocity and force vectors  0: no, 1: yes

def simulation(i, j):
    
    print '***********************************************************************'
    print '                  ELECTRICALLY CHARGED PARTICLES                       '
    print '***********************************************************************'
    print
    
    gconfig.current_time = time.clock()
    
    #Setting particle properties
    myparticles = particles(dispersion, sdistrib[j], vdistrib, npart[i], diameter, clearance, rho, icharge, eforce[j], dforce, cfl, nf)
    
    #Cartesian mesh creation
    mymesh = domain3D(meshtype, myparticles, borders, boundary, epsilon, ncelld, nf)
    mymesh.cartesian_check()
    
    #Solving electrically charged particle mouvement up to tfinal or itfinal
    mypostprocess = postprocess(myparticles, mymesh)
    myinout = inout(myparticles, it_step, pl_step, savetxt, drawplot, saveplot, showplot, tracking, vectors)
    myinit = initialization(myparticles)
    mypyecp = pyecp(tfinal, itfinal)
    mypyecp.solve(mymesh, myparticles, mypostprocess, myinout, myinit)


    print '---------------------------------------------------------------'
    print "Cpu time:" , mypyecp.t_total, "s"
    print "To solve the interaction of %d electrostatic particles for %2.5f seconds using %s algorithm" %(myparticles.npart, mypyecp.t, eforce[j])
    print 'Characteristic time of Coulomb interaction %2.5f' %tpe

    #simulation time of the electrostatic force calculation module per iteration
    simtime[i,j] = mypyecp.t_it[2]
    
    if j == 0:
        sit = myinout.it_to_sit(0)  
        outdir = './particles/txt/'
        txtname = 'p_' + myinout.suffix + '_' + sit + '.txt'
        initname = 'init_' + str(myparticles.npart) + '_' + ("mono" if myparticles.dispersion == 0 else "poly") + '.txt'
        print 'cp '+ outdir + txtname + ' ../cases/' + initname
        subprocess.call(['cp', outdir + txtname, '../cases/' + initname])

def write_complexity(npart, simtime):
          
    txtname = 'simulation_time_' + str(max(npart)) +'.txt'
    head = ['Simulation time', 'N']
    for i in np.arange(len(eforce)):
        head[1] += ', ' + eforce[i]
    h = '\n'.join(head) 
    args = [() for i in np.arange(len(npart))]

    #construction of each line
    for i in np.arange(len(npart)):
        args[i] = ('%5d'  %(npart[i]),)
        for j in np.arange(len(eforce)):
            args[i] += ('%4.5f' %(simtime[i,j]),)
    
    with open(txtname, 'w') as output:
        output.write(h+'\n')
        fmt = '{0:>5}'
        for j in np.arange(len(eforce)):
            fmt += ' {%d:>11}' %(j+1)
        fmt += '\n'
        for i in np.arange(len(npart)):
            output.write(fmt.format(*args[i]))

def plot_complexity(npart, eforce):

    txtname = 'simulation_time_' + str(max(npart)) +'.txt'
    simtime = np.empty((len(npart), len(eforce)))
    
    fig = plt.figure()
    ax = fig.add_subplot(111)
    #markers, linestyles, colours, labels
    l = [k+str(n) for k,n in zip(eforce,eforce2)]
    m  = ['o', 's', '^']
    ls = ['-', '--', ':']
    c  = ['black', 'r', 'b']
    
    for j in range(len(eforce)):
        #reading simulation times from file    
        simtime[:,j] = np.genfromtxt(txtname, usecols = (j+1), skip_header=2, unpack=True)

    for i in np.arange(len(eforce)):
        #linear regression    
        fit = np.polyfit(np.log(npart), np.log(simtime[:,i]), 1)
        l[i] += r' $\left[\mathcal{O}\left(N^{%1.2f}\right)\right]$' %(fit[0])
        # fit_fn is a function which takes in x and returns an estimate for y
        fit_fn = np.poly1d(fit)
        plt.plot(npart, simtime[:,i], marker= m[i], linestyle=ls[i], color='k', label=l[i]) #simulation time data
        plt.plot(npart, exp(fit[1])*npart**fit[0], linestyle = ls[i], color='r')            #linear regression plot for logarithmic scale
    
    plt.xlabel(r'Number of particles $N$')
    plt.ylabel(r'Simulation time (one iteration average) $T_s$')
    plt.title(r'Simulation time against number of particles for three algorithms')
    plt.xscale('log')
    plt.yscale('log')
    plt.grid()
    ax.legend(loc='best')
    
#    plt.show()
    #saving the plot
    outdir = './'
    pdfname = 'simulation_time_' + str(max(npart)) +'.pdf'
    pp = PdfPages(outdir+pdfname)
    pp.savefig(fig, bbox_inches=None)
    pp.close()
    plt.close(fig)

#------------------------------------------------------------------------------
#                     PERFORMANCE TEST CASE USING 3 ALGORITHMS
#------------------------------------------------------------------------------

simtime = np.empty((len(npart), len(eforce)))

for i in np.arange(len(npart)):
        
    mp = 4./3.*rho*np.pi*(diameter/2.)**3
    le = max((xmax-xmin),(ymax-ymin),(zmax-zmin))            #characteristic length of Coulomb's interaction    
    tpe = np.sqrt(mp*le**3/(npart[i]*gconfig.ke*icharge**2)) #characteristic time scale of Coulomb's interaction
    
    #Time and iterations configuration
    tfinal  = tpe #final time
    itfinal = 0. #final iteration

    for j in np.arange(len(eforce)):
#        pass
        simulation(i,j)
        
#---------------------------------------------write simulation times in simulation_time.txt-------------------------------------------
write_complexity(npart, simtime)
#--------------------------------plotting simulation time against number of particles for the three algorithms-----------------------
plot_complexity(npart, eforce)