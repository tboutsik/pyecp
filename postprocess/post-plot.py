# -*- coding: utf-8 -*-
"""
Created on Mon Oct 16 16:36:35 2017

Electrostatic Charged Particles test case

@author: a.boutsik
"""
import numpy as np
import math
import os
import matplotlib.pyplot as plt

from pyecp.particles import *
from pyecp.mesh  import *
#inout module contains all the rc.params for the plots
from pyecp.inout import *

#------------------------------------------------------------------------------
#                  TEST CASE SETUP STARTS AFTER THIS LINE
#------------------------------------------------------------------------------

#Particle properties
dispersion = 0                 #diameter dispersion 0: monodisperse, 1: polydisperse
sdistrib   = 'dipole'            #type of particle spatial distribution:  random, uniform, file, centered, dipole
vdistrib   = 'zero'            #type of particle velocity distribution: random, uniform, zero, file
npart      = 2                 #total number of particles
diameter   = 360*10**(-6)      #maximum diameter of particles
rho        = 910               #density of the solid phase (particles) e.g. polyethylene
ncelld     = 3                 #cell size -to- diameter ratio
clearance  = 10**(-6)*diameter #clearence between particles
icharge    = 10**(-13)         #initial particles charge
cfl        = 1.                #CFL-type condition for lagrangian tracking of particles <=2.5

#Computational domain
ldx = 500; ldy = 500; ldz = 500
xmin = -ldx*diameter ; xmax = ldx*diameter #x borders
ymin = -ldy*diameter ; ymax = ldy*diameter #y borders
zmin = -ldz*diameter ; zmax = ldz*diameter #z borders
borders = np.array([[xmin, xmax], [ymin, ymax], [zmin, zmax]])
epsilon = 0   #distance for virtual domain measured in particle dimeters--> epsilon < ld
nf = 2 #number of cells in x, y, z direction for manual

if sdistrib == 'centered':
    npart = nf**3
elif sdistrib == 'dipole':
    npart = 2

#Eulerian grid: eq_particle, poisson
meshtype = 'eq_particle'
#meshtype = 'poisson'
#meshtype = 'manual'

#boundary conditions
bcx = 0 #0: periodic, 1: wall
bcy = 0 #0: periodic, 1: wall
bcz = 0 #0: periodic, 1: wall
boundary = [bcx, bcy, bcz]

#electric force algorithm: direct, extended, optimized
eforce = 'direct'     #direct particle algorithm
#eforce = 'extended'   #extended particle mesh
#eforce = 'optimized'  #optimized particle mesh

#artificial drag (viscous dissipation of kinetic energy)
dforce = 1 #artificial drag 0: no drag, 1: drag 

#calculating characteristic time scale of Coulomb's interaction
mp = 4./3.*rho*np.pi*(diameter/2.)**3
le = min((xmax-xmin),(ymax-ymin),(zmax-zmin)) #characteristic length of Coulomb's interaction    
tpe = np.sqrt(mp*le**3/(npart*gconfig.ke*icharge**2))                #characteristic time scale of Coulomb's interaction

#Time and iterations configuration
tfinal   = 8*tpe #final time
itfinal  = 0  #final iteration

#Output options
it_step  = 5 #iteration step for which data is written in .txt files
pl_step  = 5 #iteration step for which data is plotted in .pdf files

savetxt  = 1 #option to save the txts                        0: no, 1: yes
drawplot = 1 #option to create the plots                     0: no, 1: yes
showplot = 0 #option to show the plot in the terminal        0: no, 1: yes (block=True) 2:yes (block=False)
saveplot = 1 #option to save the plots                       0: no, 1: yes
tracking = 0 #option to plot the trajectory of the particles 0: no, 1: yes
vectors  = 1 #option to plot the velocity and force vectors  0: no, 1: yes

#------------------------------------------------------------------------------
#--------------------Create xp(t), yp(t), zp(t) graphs-------------------------
#------------------------------------------------------------------------------

class FixedOrderFormatter(ScalarFormatter):
    """Formats axis ticks using scientific notation with a constant order of 
    magnitude"""
    def __init__(self, order_of_mag=0, useOffset=True, useMathText=False):
        self._order_of_mag = order_of_mag
        ScalarFormatter.__init__(self, useOffset=useOffset, 
                                 useMathText=useMathText)
    def _set_orderOfMagnitude(self, range):
        """Over-riding this to avoid having orderOfMagnitude reset elsewhere"""
        self.orderOfMagnitude = self._order_of_mag

def position_plot():

    outdir = './particles/txt/'
    if eforce != 'direct':
        txt = 'p_' + myinout.suffix + '_' + str(mymesh.nf)
    else:
        txt = 'p_' + myinout.suffix

    itfinal  = 0    
    #finding the .txt file of the last iteration in folder outdir
    for file in os.listdir(outdir):
        if file.startswith(txt):
            it = int(file[len(txt)+1:len(txt)+7])
            if it > itfinal: 
                itfinal = it

    #creating iiterations array    
    it = np.arange(0,itfinal,it_step)
    it = np.append(it,itfinal)
    numberoftxt = len(it)

    #creating time array, initializing position arrays
    t = np.empty(numberoftxt)
    xp = np.empty((npart,numberoftxt))
    yp = np.empty((npart,numberoftxt))
    zp = np.empty((npart,numberoftxt))

    #for every file we read the second line to get the physical time and then we read the positions of all particles at that instant 
    for i in range(len(it)):
    
        sit = myinout.it_to_sit(it[i])
        txtname = txt + '_' + sit + '.txt'
        with open(outdir+txtname) as f:
            time = f.readlines()[1]
        t[i] = float(time[6:])
        
        xp[:,i], yp[:,i], zp[:,i] = np.genfromtxt(outdir+txtname, usecols = (0,1,2), skip_header=2, unpack=True)
            
    fig, axes = plt.subplots(nrows=3, ncols=1, sharex=True, figsize=(13,11))
    plt.xlabel(r'Time $t$ $(s)$')
    #plt.title('Particles position vs time')
    
    #color=iter(cm.rainbow(np.linspace(0,1,npart)))
    clrs_list=['k','r','b','g'] # list of basic colors
    styl_list=['-','--','-.',':'] # list of basic linestyles

    #creating 3 plots of x,y,z for 4 at most particles
    c = -1
    s = -1
    
    if npart > 4:
        ip = npart / 4
    else:
        ip = 1
    
    #final list of particles to plot their positions
    rng = range(0, npart, ip)
    if npart-1 not in rng:
        rng.append(npart-1)
        
    for p in rng:

        #little code to iterate through line colors and styles to make sure every particle has a unique set        
        s += 1
        if s % 4 == 0: c += 1
        clrr=clrs_list[c % 4]
        styl=styl_list[s % 4]
    
        axes[0].set_title(r'$x$-position',fontsize=axisfontsize)
        axes[0].set_ylabel(r'$x_p$ $(m)$',fontsize=axisfontsize)
        axes[0].plot(t, xp[p,:], label = 'p '+str(p), c=clrr, ls=styl)
        start, end = axes[0].get_ylim()
        step = (end-start)/4.
        end = end+step
        axes[0].set_yticks(np.arange(start, end, step))
        axes[0].yaxis.set_major_formatter(FixedOrderFormatter(-3))
    
        axes[1].set_title(r'$y$-position',fontsize=axisfontsize)  
        axes[1].set_ylabel(r'$y_p$ $(m)$',fontsize=axisfontsize)
        axes[1].plot(t, yp[p,:], label = 'p '+str(p), c=clrr, ls=styl)
        start, end = axes[1].get_ylim()
        step = (end-start)/4.
        end = end+step
        axes[1].set_yticks(np.arange(start, end, step))
        axes[1].yaxis.set_major_formatter(FixedOrderFormatter(-3))
    
        axes[2].set_title(r'$z$-position',fontsize=axisfontsize)
        axes[2].set_ylabel(r'$z_p$ $(m)$',fontsize=axisfontsize)
        axes[2].plot(t, zp[p,:], label = 'p '+str(p), c=clrr, ls=styl)
        start, end = axes[2].get_ylim()
        step = (end-start)/4.
        end = end+step
        axes[2].set_yticks(np.arange(start, end, step))
        axes[2].yaxis.set_major_formatter(FixedOrderFormatter(-3))
    
    axes[0].legend(loc = 'upper right')
    axes[1].legend(loc = 'upper right')
    axes[2].legend(loc = 'upper right')
    
    mng = plt.get_current_fig_manager()
    ### works on Ubuntu??? >> did NOT working on windows
    mng.resize(*mng.window.maxsize())
    #mng.window.state('zoomed') #works fine on Windows!
#    plt.show()

    outdir = './post_process/'    
    pdfname = 'positions_' + myinout.suffix + '.pdf'
    pp = PdfPages(outdir+pdfname)
    pp.savefig(fig, bbox_inches='tight')
    pp.close()
    plt.close(fig)

def correl_plot():
    
    outdir = './post_process/'
    txtname = 'correl_' + myinout.suffix + '.txt'
    
    with open(outdir+txtname) as f:
        t, Rp = np.genfromtxt(outdir+txtname, usecols = (1,5), skip_header=2, unpack=True)

    #finding the index where the system is considered stable    
    flag = False
    i = 0
    istable = 0
    while i in range(len(Rp)) and not flag:
        if not math.isnan(Rp[i]):
            istable = i
            flag = True
        i += 1

    fig = plt.figure(figsize=(10.5,7))
    #colours and labels for the 6 error zones

    plt.plot(t, Rp, c = 'k', ls = ':', lw = 1)
    plt.xlabel(r'Time $t$ $(s)$',fontsize=axisfontsize)
    plt.ylabel(r'Auto-correlation function $R_p$',fontsize=axisfontsize)
    
    plt.grid()
    
#    plt.show()

    pdfname = 'correl_' + myinout.suffix + '.pdf'
    pp = PdfPages(outdir+pdfname)
    pp.savefig(fig, bbox_inches='tight')
    pp.close()
    plt.close(fig)
    
    return istable, flag

def magnitude(x):
    return int(math.log10(x))

def energy_plot(istable, flag):
    
    outdir = './post_process/'
    txtname = 'energy_' + myinout.suffix + '.txt'
    
    with open(outdir+txtname) as f:
        t, Ue, Ek, Et = np.genfromtxt(outdir+txtname, usecols = (1,2,3,4), skip_header=2, unpack=True)

    fig, ax = plt.subplots(figsize=(10.5,7))
    labels = [r'Electric potential $U_e$', r'Kinetic energy $E_k$', r'Total energy $E_t$']
    
    every = int(np.ceil(len(t)/10))
    
    plt.plot(t, Ue, c = 'b', marker = 's', markevery=every, ls = ':',  lw = 2)
    plt.plot(t, Ek, c = 'r', marker = 'o', markevery=every, ls = '--', lw = 2)
    plt.plot(t, Et, c = 'k', marker = 'p', markevery=every, ls = '-',  lw = 2)
    
#    if flag: plt.axvline(x=t[istable], c = 'g', ls = '-.',  lw = 1)
    
#    #add a xtick for the time after which the system is considered stable
#    list_loc=list(ax.xaxis.get_majorticklocs())
#    if flag and t[istable] not in list_loc:
#        list_loc.append(t[istable])
#        list_loc.sort()
#        ax.xaxis.set_ticks(list_loc)
    
    plt.xlabel(r'Time $t$ $(s)$', fontsize=axisfontsize)
    plt.ylabel(r'Energy $E$ $(J)$', fontsize=axisfontsize)
#    m = magnitude(min(min(Ue),min(Ek)))
#    plt.ylim(ymin = -10.**m)
    plt.ylim(ymin = -min(Ue)/10)
    
    plt.legend(labels, loc='best')
    plt.grid()

    ax.xaxis.set_major_formatter(FormatStrFormatter("%d"))
#    plt.show()

    pdfname = 'energy_' + myinout.suffix + '.pdf'
    pp = PdfPages(outdir+pdfname)
    pp.savefig(fig, bbox_inches='tight')
    pp.close()
    plt.close(fig)

print '***********************************************************************'
print '                  PLOTTING PARTICLE POSITION VS TIME                   '
print '***********************************************************************'
print

#Setting particle properties
myparticles = particles(dispersion, sdistrib, vdistrib, npart, diameter, clearance, rho, icharge, eforce, dforce, cfl, nf)
#Cartesian mesh creation
mymesh = domain3D(meshtype, myparticles, borders, boundary, epsilon, ncelld, nf)
#Solving electrically charged particle mouvement up to tfinal or itfinal
myinout = inout(myparticles, it_step, pl_step, savetxt, drawplot, saveplot, showplot, tracking, vectors)
    
position_plot()
istable, flag = correl_plot()
energy_plot(istable, flag)