#!/bin/bash

#-delay is the framerate (fps)
#-density is the quality, higher means better quality, 100 is usually good
#-loop is number of loops, 1 means one loop, 0 means infinite

convert -delay 10 -loop 1 -density 100 $1* $1.gif
mv $1.gif .
