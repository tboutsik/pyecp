#!/bin/bash

#-r is the framerate (fps)
#-crf is the quality, lower means better quality, 15-25 is usually good
#-s is the resolution
#-pix_fmt yuv420p specifies the pixel format, change this as needed
#-start_number specifies what image to start at
#-vframes 1000 specifies the number frames/images in the video

ffmpeg -r 25 -f image2 -s 1920x1080 -i $1_%*.png -vcodec libx264 -crf 25 -pix_fmt yuv420p $1.mp4
mv $1.mp4 .
