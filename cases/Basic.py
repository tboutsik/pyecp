# -*- coding: utf-8 -*-
"""
Created on Mon Oct 16 16:36:35 2017

Electrostatic Charged Particles test case

@author: a.boutsik
"""
import time
import numpy as np
import subprocess
import matplotlib.pyplot as plt
#from matplotlib.pyplot import cm 

from pyecp.mesh  import *
from pyecp.particles import *
from pyecp.gconfig import *
from pyecp.pyecp import *
from pyecp.postprocess import *
from pyecp.inout import *
from pyecp.initialization import *

#------------------------------------------------------------------------------
#                  TEST CASE SETUP STARTS AFTER THIS LINE
#------------------------------------------------------------------------------

#Particle properties
dispersion = 0                 #diameter dispersion 0: monodisperse, 1: polydisperse
sdistrib   = 'random'          #type of particle spatial distribution:  random, uniform, file, centered
vdistrib   = 'zero'            #type of particle velocity distribution: random, uniform, zero, file
npart      = 80                #total number of particles
diameter   = 360*10**(-6)      #maximum diameter of particles
rho        = 910               #density of the solid phase (particles) e.g. polyethylene
ncelld     = 3                 #cell size -to- diameter ratio
clearance  = 10**(-6)*diameter #clearence between particles
icharge    = 10**(-13)         #initial particles charge
cfl        = 1.                #CFL-type condition for lagrangian tracking of particles <=2.5

#Computational domain
ldx = 500; ldy = 500; ldz = 500
xmin = -ldx*diameter ; xmax = ldx*diameter #x borders
ymin = -ldy*diameter ; ymax = ldy*diameter #y borders
zmin = -ldz*diameter ; zmax = ldz*diameter #z borders
borders = np.array([[xmin, xmax], [ymin, ymax], [zmin, zmax]])
epsilon = 350   #distance for virtual domain measured in particle dimeters--> epsilon < ld
nf = 7 #number of cells in x, y, z direction for manual

#Eulerian grid: eq_particle, poisson
meshtype = 'eq_particle'
#meshtype = 'poisson'
#meshtype = 'manual'

#boundary conditions
bcx = 0 #0: periodic, 1: wall
bcy = 0 #0: periodic, 1: wall
bcz = 0 #0: periodic, 1: wall
boundary = [bcx, bcy, bcz]

#electric force algorithm: direct, extended, optimized
#eforce = 'direct'      #direct particle algorithm
eforce = 'extended'   #extended particle mesh
#eforce = 'optimized'  #optimized particle mesh

#artificial drag (viscous dissipation of kinetic energy)
dforce = 1 #artificial drag 0: no drag, 1: drag 

#calculating characteristic time scale of Coulomb's interaction
mp = 4./3.*rho*np.pi*(diameter/2.)**3
le = min((xmax-xmin),(ymax-ymin),(zmax-zmin))         #characteristic length of Coulomb's interaction    
tpe = np.sqrt(mp*le**3/(npart*gconfig.ke*icharge**2)) #characteristic time scale of Coulomb's interaction

#Time and iterations configuration
tfinal   = 8*tpe #final time
itfinal  = 0   #final iteration

#Output options
it_step  = 5 #iteration step for which data is written in .txt files
pl_step  = 5 #iteration step for which data is plotted in .pdf files

savetxt  = 1 #option to save the txts                        0: no, 1: yes
drawplot = 1 #option to create the plots                     0: no, 1: yes
showplot = 0 #option to show the plot in the terminal        0: no, 1: yes (block=True) 2:yes (block=False)
saveplot = 1 #option to save the plots                       0: no, 1: yes
tracking = 0 #option to plot the trajectory of the particles 0: no, 1: yes
vectors  = 1 #option to plot the velocity and force vectors  0: no, 1: yes

#------------------------------------------------------------------------------
#                     ELECTRICALLY CHARGED PARTICLES
#------------------------------------------------------------------------------

print '***********************************************************************'
print '                  ELECTRICALLY CHARGED PARTICLES                       '
print '***********************************************************************'
print

gconfig.current_time = time.clock()

#Setting particle properties
myparticles = particles(dispersion, sdistrib, vdistrib, npart, diameter, clearance, rho, icharge, eforce, dforce, cfl, nf)

#Cartesian mesh creation
mymesh = domain3D(meshtype, myparticles, borders, boundary, epsilon, ncelld, nf)
mymesh.cartesian_check()

#Solving electrically charged particle mouvement up to tfinal or itfinal
mypostprocess = postprocess(myparticles, mymesh)
myinout = inout(myparticles, it_step, pl_step, savetxt, drawplot, saveplot, showplot, tracking, vectors)
myinit = initialization(myparticles)
mypyecp = pyecp(tfinal, itfinal)
mypyecp.solve(mymesh, myparticles, mypostprocess, myinout, myinit)

print '-----------------------------------------------------------------------------------'
print "To solve the interaction of %d electrostatic particles for %2.5f seconds using %s algorithm" %(myparticles.npart, mypyecp.t, eforce)
print 'Characteristic time of Coulomb interaction %2.5f' %tpe
print

if sdistrib != 'file' and sdistrib != 'uniform': 
    sit = myinout.it_to_sit(0)  
    outdir = './particles/txt/'
    if eforce != 'direct':
        txtname = 'p_' + myinout.suffix + '_' + str(mymesh.nf) + '_' + sit + '.txt'
    else:
        txtname = 'p_' + myinout.suffix + '_' + sit + '.txt'          
    initname = 'init_' + str(myparticles.npart) + '_' + ("mono" if myparticles.dispersion == 0 else "poly") + '.txt'
    print 'cp '+ outdir + txtname + ' ../cases/' + initname
    subprocess.call(['cp', outdir + txtname, '../cases/' + initname])