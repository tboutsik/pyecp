# -*- coding: utf-8 -*-
"""
Created on Thu Nov 16 15:49:07 2017

@author: aboutsik
"""

import numpy as np
import math
import os
from pylab import *
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes
from mpl_toolkits.axes_grid1.inset_locator import mark_inset
from matplotlib.backends.backend_pdf import PdfPages
from matplotlib.ticker import ScalarFormatter, FormatStrFormatter

import matplotlib as mpl
axisfontsize = 22
mpl.rcParams['xtick.labelsize'] = axisfontsize
mpl.rcParams['ytick.labelsize'] = axisfontsize
#mpl.rcParams['font.size'] = axisfontsize
#mpl.rcParams['legend.fontsize'] = 15
#mpl.rcParams['figure.titlesize'] = 'medium'
mpl.rcParams['text.usetex'] = True
mpl.rcParams['font.family'] = 'serif'

class FixedOrderFormatter(ScalarFormatter):
    """Formats axis ticks using scientific notation with a constant order of 
    magnitude"""
    def __init__(self, order_of_mag=0, useOffset=True, useMathText=False):
        self._order_of_mag = order_of_mag
        ScalarFormatter.__init__(self, useOffset=useOffset, 
                                 useMathText=useMathText)
    def _set_orderOfMagnitude(self, range):
        """Over-riding this to avoid having orderOfMagnitude reset elsewhere"""
        self.orderOfMagnitude = self._order_of_mag

def cost(N,Nf):
    return N*(27*N/Nf**3+Nf**3-27)

def optimal(N):
    Nf = np.ceil(np.sqrt(3)*N**(1./6.))

    nceil  = int(math.ceil(Nf))
    nfloor = int(math.floor(Nf))
    
    #cost of particle-mesh algorithm
    cceil  = cost(nceil,  N)   
    cfloor = cost(nfloor, N)
    
    if cceil < cfloor: #choosing the integer that minimizes the computational cost
        Nf = nceil
    else: #cceil <= cfloor
        Nf = nfloor
        
    fopt = cost(N,Nf)

    return Nf, fopt
    

outdir = './post_process/'

clrs_list=['b','r','k','g'] # list of basic colors
styl_list=['-','--'] # list of basic linestyles
m = ['o','s','D']

fig, ax = plt.subplots(figsize=(11,8))

axins = zoomed_inset_axes(ax, 7.5, loc=1, bbox_to_anchor=(0.7, 0.64), bbox_transform=ax.figure.transFigure) # zoom-factor: 2.5, location: upper-left
x1, x2, y1, y2 = 3.9, 5.1, -0.2*10**5, 1*10**5 # specify the limits
axins.set_xlim(x1, x2) # apply the x-limits
axins.set_ylim(y1, y2) # apply the y-limits

N = np.array([10 * 2**i for i in range(2,8)])
Nf = np.array([i for i in range(2,17)]) #number of cells in x, y, z direction for manual

l = [r'$N=%d$' %n for n in N]
#colours and labels for the 6 error zones

c = -1
s = -1

for i in range(len(N)):
    
    Nfopt, fopt = optimal(N[i])
    
    #little code to iterate through line colors and styles to make sure every particle has a unique set        
    s += 1
    if s % 2 == 0: c += 1
    clrr=clrs_list[c % 4]
    styl=styl_list[s % 2]
    
    f = cost(N[i],Nf)
    markers_on = [list(Nf).index(Nfopt)]
    ax.plot(Nf, f, c=clrr,  marker = m[c], markevery = markers_on, markersize = 5, ls=styl, lw = 1, label=l[i])

    axins.plot(Nf, f, Nf, f, c=clrr, marker = m[c], markevery = markers_on, markersize = 5, ls=styl, lw = 1)
    
plt.yticks(visible=False)
plt.xticks([4,5], visible=True)

mark_inset(ax, axins, loc1=3, loc2=4, fc="none", ec="0.5")

ax.set_title(r'Computational cost vs Number of particles',fontsize=axisfontsize)

ax.set_xlabel(r'Number of cells $N_f$',fontsize=axisfontsize)
ax.set_ylabel(r'Computational cost $f(N_f)$',fontsize=axisfontsize)
ax.yaxis.set_major_formatter(FixedOrderFormatter(7))

# Make the x-axis ticks formatted to 0 decimal places
ax.xaxis.set_major_formatter(FormatStrFormatter('%d'))
ax.legend(loc='best')
ax.grid()    
#    plt.show()

pdfname = 'cost_theory.pdf'
pp = PdfPages(pdfname)
pp.savefig(fig, bbox_inches='tight')
pp.close()
plt.close(fig)