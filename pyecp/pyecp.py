# -*- coding: utf-8 -*-
"""
Created on Mon Oct 30 14:03:29 2017

@author: aboutsik
"""
import time
from pylab import *
import numpy as np

import gconfig

class pyecp():
    "class that manages the pyecp code"

    def __init__(self, tfinal, itfinal):
        "particles class attributes initialization"
        
        self.tfinal    = tfinal
        self.itfinal   = itfinal
        self.tinterval = np.zeros(7)  #time interval for all 
        self.txt       = 0            #counter for data files
        self.pl        = 0            #counter for generated plot files
        self.qp        = np.array([]) #particle agitation array in time
    
    def solve(self, mesh, particles, postprocess, inout, init):
        "performs all operations of pyecp code during one time step"
        
        self.t  = 0.
        it = 0
        while (self.t <= self.tfinal and it <= self.itfinal):
            
            print
            print '==================================================================='
            print
            print 'Iteration = %d : Time = %1.5f s' %(it, self.t)
            print
            print '==================================================================='
            #------------------------------Particles positioning------------------------------
            particles.motion(mesh, self.t, init)

            if self.t == 0.:
                s = '0. To place %d %s particles respecting domain boundaries and radius' %(particles.npart, particles.sdistrib)
                self.time_interval(0, s)
            else:
                s = '1. To update the position and velocity of %d particles' %(particles.npart)
                self.time_interval(1, s)
        
            #------------------------------Particles electrostatic charging------------------------------
            particles.charge(self.t, init)
        
            #------------------------------Particles force calculation------------------------------ 
            particles.force(mesh, self)

            #------------------------------Post-processing of particle interaction------------------------------    
            qp2 = postprocess.energy(particles, it, self.t)
            self.qp = np.append(self.qp,qp2)
#            if t >= 8*particles.tpe
            
            s= "4. To post-process the particle interaction"
            self.time_interval(4, s)
            print 'Potential energy  = %1.5e J' %(postprocess.Ue) 
            print 'Minimum potential = %1.5e J' %(postprocess.Uemin)
            print 'Kinetic energy    = %1.5e J' %(postprocess.Ek)
            print 'Total energy      = %1.5e J' %(postprocess.Et)
            print 'Particle auto-correlation function = %1.5e' %(postprocess.rp)
            
            #------------------------------Writing particles properties in a file for 1st iteration, every it_step, and last iteration------------------------------
            if inout.savetxt == 1:
                #saves txt file for 1st iteration, every it_step, and last iteration
                if (it == 0) or (it % inout.it_step == 0) or (self.t == self.tfinal) or (it == self.itfinal):
                    self.txt += 1
                    #different outputs for the /particles
                    particles.write_distrib(mesh, inout, self.t, it)
                    postprocess.write_postprocess(inout, particles, self.t, it)
                    
                    if particles.eforce != 'direct':
                        self.txt += 1
                        #output for the /cells
                        mesh.write_cells(inout, particles, self.t, it)
            
                    s = "5. To create the output files"
                    self.time_interval(5, s)        
            
            #------------------------------Plotting particles------------------------------ 
            if inout.drawplot == 1:
                #plots for 1st iteration, every pl_step, and last iteration
                if (it == 0) or (it % inout.pl_step == 0) or (self.t == self.tfinal) or (it == self.itfinal):
                    self.pl += 1
                    # 3D Plot
                    inout.eplot(mesh, particles, postprocess, it, self.t)
                    
                    s = "6. To generate the plots"
                    self.time_interval(6, s)               

            #------------------------------Time update------------------------------
            #stop updating iterations and time if final time is reached or final iteration is reached
            if (self.t != self.tfinal and it != self.itfinal):
                it += 1   
                if self.t + particles.dt > self.tfinal:
                    particles.dt = self.tfinal-self.t
                self.t += particles.dt
            else: #if t == final
                break
        
        #------------------------------successful end of time iterations------------------------------
        self.time_total(it)
#        if it!=0: inout.position_plot(it, particles, mesh)
    
    def time_interval(self, i, s):
        "calculates and prints the time interval for each module for each iteration"
        
        #calculating local time interval for module
        tinterval = time.clock() - gconfig.current_time
        gconfig.current_time = time.clock()
        print
        print '----------------------------------------------------------'
        print "Cpu time = %.5f s" %(tinterval)
        print s
        print '----------------------------------------------------------'
        #calculating total time interval for module
        self.tinterval[i] += tinterval

    def time_total(self, it):
        "calculates and prints total computational time of all simulation and of each module"

        self.t_total = np.sum(self.tinterval)      #total simulation time
        t_rel = 100*self.tinterval/self.t_total    #simulation time of each module relative to total simulation time
        self.t_it  = self.tinterval/(it+1)         #simulation time of each module per iteration
        print
        print '=========================SIMULATION SUCCESSFULLY COMPLETED========================='
        #Printing total simulation time excluding initialization (it = 0)
        print 'Total simulation time = %1.5f s for %d iterations (+ %2.5f s for initialization)' %(np.sum(self.tinterval[1:]), it+1, self.tinterval[0])           
        print 'Simulation time per iteration (excluding initialization) = %1.5f s/it' %(np.sum(self.tinterval[1:])/(it+1))
        print '***********************************************************************************'
        print 'Analysis of absolute and relative simulation time for every major module'
        print '***********************************************************************************'
        print '                T: Total | R: Relative | IT: Per iteration'
        print

        args = [() for i in range(len(self.tinterval))]
        args[0] = ('0. Particle initialization',                 ': T %2.2e s | R' %(self.tinterval[0]), '%2.2f%%'      %(t_rel[0])                         )
        args[1] = ('1. Particle position & velocity update',     ': T %2.2e s | R' %(self.tinterval[1]), '%2.2f%% | IT' %(t_rel[1]), '%2.2e s/it' %(self.t_it[1]))
        args[2] = ('2. Calculation of electrostatic forces',     ': T %2.2e s | R' %(self.tinterval[2]), '%2.2f%% | IT' %(t_rel[2]), '%2.2e s/it' %(self.t_it[2]))
        args[3] = ('3. Calculation of drag forces',              ': T %2.2e s | R' %(self.tinterval[3]), '%2.2f%% | IT' %(t_rel[3]), '%2.2e s/it' %(self.t_it[3]))
        args[4] = ('4. Post-processing of particle interaction', ': T %2.2e s | R' %(self.tinterval[4]), '%2.2f%% | IT' %(t_rel[4]), '%2.2e s/it' %(self.t_it[4]))
        args[5] = ('5. Creation of %d output files' %(self.txt), ': T %2.2e s | R' %(self.tinterval[5]), '%2.2f%% | IT' %(t_rel[5]), '%2.2e s/it' %(self.t_it[5]))
        args[6] = ('6. Generation of %d plots'      %(self.pl) , ': T %2.2e s | R' %(self.tinterval[6]), '%2.2f%% | IT' %(t_rel[6]), '%2.2e s/it' %(self.t_it[6]))
        
        for i in range(len(self.tinterval)):
            if i == 0:         
                print '{0:<42} {1:>15} {2:>6}'.format(*args[i])
            else:
                print '{0:<42} {1:>15} {2:>11} {3:>10}'.format(*args[i])