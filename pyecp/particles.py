# -*- coding: utf-8 -*-
"""
Created on Tue Oct 17 17:40:23 2017

@author: a.boutsik
"""
import time
import numpy as np

import gconfig
import mesh

class particles():
    "calculate particle forces, position, velocity"
    
    global ke, eps
    ke  = gconfig.ke
    eps = gconfig.eps
    
    def __init__(self, dispersion, sdistrib, vdistrib, npart, diameter, clearance, rho, icharge, eforce, dforce, cfl, nf):
        "particles class attributes initialization"

        #setting particle number according to desired distribution
        if sdistrib == 'uniform': 
            npart = self.uniform_npart(npart)
        elif sdistrib == 'centered':
            npart = self.centered_npart(nf)

        #particle properties  
        self.npart = npart                  #number of particles            
        self.dispersion = dispersion        #diameter dispersion 0: monodisperse, 1: polydisperse
        self.sdistrib   = sdistrib          #type of particle spatial distribution:  random, uniform
        self.vdistrib   = vdistrib          #type of particle velocity distribution: random, uniform
        self.diameter   = diameter          #maximum particle diameter
        self.radius     = diameter/2.       #maximum particle radius
        self.rmin       = 50*10**(-6)       #minimum radius allowed for the polydisperse distribution
        self.clearance  = clearance         #clearance between particles
        self.rho        = rho               #particle density
        self.icharge    = icharge           #initial particles charge
        self.eforce     = eforce            #electric force algorithm: direct, extended p-m, optimized p-m
        self.dforce     = dforce            #artificial drag boolean trigger
        self.cfl        = cfl               #CFL-type condition for langrangian tracking of particles

        #particle position
        self.xp = np.zeros(self.npart)      #particles x position vector
        self.yp = np.zeros(self.npart)      #particles y position vector
        self.zp = np.zeros(self.npart)      #particles z position vector
        #particle velocity
        self.up = np.zeros(self.npart)      #particles u velocity vector
        self.vp = np.zeros(self.npart)      #particles v velocity vector
        self.wp = np.zeros(self.npart)      #particles w velocity vector
        self.rp = np.zeros(self.npart)
        #particle properties
        self.r  = np.zeros(self.npart)      #particles radius
        self.mp = np.zeros(self.npart)      #particles mass
        self.ve = np.zeros((self.npart, 3)) #velocity vectors for each particle
        self.Q  = np.empty(self.npart)      #particle charge
        #particle forces
        self.fe = np.zeros((self.npart, 3)) #total electrostatic force for each particle
        self.fd = np.zeros((self.npart, 3)) #drag force for each particle (form of dissipation)
        self.ft = np.zeros((self.npart, 3)) #total force for each particle

        #particle positioning boolean arrays
        self.outofbounds   = np.empty((self.npart,             3), dtype=bool) #out of the domain
        self.superposition = np.empty((self.npart, self.npart, 3), dtype=bool) #superposition with other particles

        #post-processing quantities
        self.Ue = 0. #electric  potential energy 
        self.Ek = 0. #particles kinetic   energy
        self.E  = 0. #inertia-to-electrostatic forces ratio

        #virtual fluid properties
        if dforce:
            self.rhof = 1.229                      #fluid density
            self.muf  = 1.73*10**(-5)              #fluid dynamic   viscosity                                               
            self.nuf  = self.muf/self.rhof         #fluid kinematic viscosity
            self.uf   = np.zeros((self.npart, 3))  #fluidization velocity
#            self.uf[:,2] = 3.5*10**(-5)           #along z axis
            
    def uniform_npart(self, npart):
        "sets particle number for uniform distribution"
                
        if npart < 8: # 8=2**3 which is the smallest cube possible
            print 'Given number of particles %d is too small' %(npart)
            npart = 8
        else: #npart >=8
            if not self.is_power(npart,3):
                print 'Given number of particles %d is not a perfect cube' %(npart)
                npd = int(np.floor(npart**(1./3.))) #not sure that this will always be consistent with the number calculated by the solution of the equidistance problem 
                npart = npd**3
                print 'For equidistant placement, the number of particles has to be %d' %(npart)
            else:
                print 'Given number of particles %d is a perfect cube' %(npart)
        
        return npart

    def centered_npart(self, nf):
        "sets particle number for centered distribution"

        nx = ny = nz = nf
        npart = nx*ny*nz
        
        return npart

    def is_power(self, N, n):
        "checks if N is a prefect power of n"
        r = int(round(N ** (1.0 / n)))
        return r**n == N
        
    def motion(self, m, t, init):
        "solves the equation of motion of all Lagrangian particles per time step"
        
        if t == 0:
            #placing the particles in a virtual domain slighlty smaller than the computational domain to avoid singularities due to periodicity 
            virtualm = mesh.virtual(m)
            init.distrib(virtualm, m, self)
            
            #characteristic scales
            self.lp = min(self.cfl * 2 * self.r)                                      #maximum distance that a particle can travel between two time steps
            self.tpe = np.sqrt(max(self.mp)*m.le**3/(self.npart*ke*self.icharge**2))  #characteristic time scale of Coulomb's interaction needed to uncorrelate the particle velocity 
            
            xp = self.xp  #test
            yp = self.yp  #test
            zp = self.zp  #test
            
        else: #t>0
            #calculate new particle velocity
            self.up = self.ft[:,0]/self.mp*self.dt + self.up
            self.vp = self.ft[:,1]/self.mp*self.dt + self.vp
            self.wp = self.ft[:,2]/self.mp*self.dt + self.wp

            #calculate new particle position without periodicity
            xp = self.up*self.dt + self.xp #test
            yp = self.vp*self.dt + self.yp #test
            zp = self.wp*self.dt + self.zp #test
                        
            self.xp = xp
            self.yp = yp
            self.zp = zp

            #if at least one boundary condition is periodic
            if not np.all(m.boundary):
                self.position_periodicity(m)
            
        self.ve = np.array([self.up, self.vp, self.wp]).transpose()
        self.ven = np.array([np.linalg.norm(self.ve[i,:]) for i in range(self.npart)]) #norms of all ve vectors
        self.Ek = 0.5*np.sum(self.mp*self.ven**2)                                      #kinetic energy of all particles

        #time step calculation        
        self.time_step(m)
        
        #checking if any particle is ready to exit the computational domain  #test#test#test#test#test#test
        out = 0.
        for i in range(self.npart):
            outofbounds = self.outofdomain(xp[i], yp[i], zp[i], m.borders)
            if np.any(outofbounds):
                out+=1
        print '%d particles re-enter the periodic domain' %out

    def position_periodicity(self, m):
        "implements position periodicity when particles surpass domain borders"

        #calculate new particle position with periodicity            
        for i in range(self.npart):
            
            if m.bcx == 0:

                if self.xp[i] > m.xmax:
                    self.xp[i] = m.xmin + (self.xp[i]-m.xmax) % m.lx
                elif self.xp[i] < m.xmin:
                    self.xp[i] = m.xmax - (m.xmin-self.xp[i]) % m.lx

            if m.bcy == 0:

                if self.yp[i] > m.ymax:
                    self.yp[i] = m.ymin + (self.yp[i]-m.ymax) % m.ly
                elif self.yp[i] < m.ymin:
                    self.yp[i] = m.ymax - (m.ymin-self.yp[i]) % m.ly

            if m.bcz == 0:

                if self.zp[i] > m.zmax:
                    self.zp[i] = m.zmin + (self.zp[i]-m.zmax) % m.lz
                elif self.zp[i] < m.zmin:
                    self.zp[i] = m.zmax - (m.zmin-self.zp[i]) % m.lz


    def time_step(self, mesh):
        "calculates the particles equation of motion time step"

        venmin  = np.min(self.ven)
        venmean = np.mean(self.ven)
        venmax  = np.max(self.ven)
        print 'min  particle velocity = %1.5e m/s' %venmin        
        print 'mean particle velocity = %1.5e m/s' %venmean
        print 'max  particle velocity = %1.5e m/s' %venmax
        print
        
        dtmax = 10**(0)
        if venmax <= self.lp/dtmax:    #this means that dt >= dtmax
            self.dt = dtmax            #an arbitrary time step in order to avoid calculating a huge time step due to very small velocity
        else:                          #this means that dt < 10
            self.dt = self.lp/venmax   #time step for particle motion equation as minimum displacement of all particles divided by maximum particle velocity

        self.E = venmean/self.icharge * np.sqrt(np.mean(self.mp)*mesh.le/(ke*self.npart)) #inertia-to-electrostatic forces ratio
        
        print 'inertia-to-electrostatic forces ratio         = %1.5e'   %(self.E)
        print 'electrostatic interaction characteristic time = %1.5e s' %(self.tpe)
        print 'particle time step for Lagrangian solver      = %1.5e s' %(self.dt)
        print

    def charge(self, t, init): #maybe in the future mesh should be also an argument
        "calculates the charge of all particles per time step"
        
        if t == 0:
            init.charge(self)
        else:
            #calculate new particle charge (if charge exchange is enabled between particles)
            pass 
            
    def force(self, mesh, pyecp):
        "calculates the ensemble of forces that are exerted on all particles"

        #calculating electric force and potential energy
        self.fe, self.Ue = self.electro_force(mesh)
        s = "2.1 To calculate the electrostatic force of %d particles using the %s algorithm" %(self.npart,  self.eforce)
        pyecp.time_interval(2, s) 
        
        #calculation of artificial drag force
        if self.dforce: 
            self.fd = self.drag_force() #resetting forces to zero
            s = "2.2 To calculate the drag force of %d particles" %(self.npart)
            pyecp.time_interval(3, s)

        self.ft = self.fe + self.fd
        
    def drag_force(self):
        "calculates the drag force that is exerted on all particles"

        fd = np.zeros((self.npart, 3)) #resetting forces to zero
        #calculation of artificial relative velocity (between fluid and particles)
        vr = self.uf - self.ve                                                   #artificial relative velocity
        vrn = np.array([np.linalg.norm(vr[i,:]) for i in range(self.npart)]) #norms of all vr vectors     
        
        #if no vrn is zero
        if np.all(vrn):
            Rep = 2.*self.r*vrn/self.nuf
            Cdp = 24./Rep*(1.+0.15*Rep**0.687)
            fdd = 0.5*Cdp*np.pi*self.r**2*self.rhof*vrn
            for i in range(3):
                fd[:,i] = fdd*vr[:,i]
        
        return fd

    def electro_force(self, mesh):
        "calculates the electrical force that is exerted on all particles"
        
        if self.eforce == 'direct':
            #Calculation of particles electrostatic forces using the direct algorithm
            fe, Ue = self.electro_force_direct(mesh)
        
        elif self.eforce == 'extended':
            #Calculation of particles electrostatic forces using the extended particle-mesh algorithm
            fe, Ue = self.electro_force_extended_particle_mesh(mesh)

        elif self.eforce == 'optimized':
            #Calculation of particles electrostatic forces using the extended particle-mesh algorithm
            fe, Ue = self.electro_force_optimized_particle_mesh(mesh)
        
        return fe, Ue
        
    def electro_force_direct(self, mesh):
        "calculates the total electrostatic (Coulomb) force for every particle using the direct algorithm"

        fe = np.zeros((self.npart, 3)) #resetting forces to zero
        Ue = 0.                        #resetting electric potential to zero

        oper = 0    
        cells = 0
        for p in range(self.npart):
            for q in range(self.npart):
                if p!=q:
                    rpq = self.distance(p, q)
                    rpqterm, potent, ndom = self.electro_dipole(mesh, rpq)
                    
                    periodicity = self.electro_periodicity(mesh, rpq)
                    rpqterm += periodicity[0]        
                    potent  += periodicity[1]        
                    ndom    += periodicity[2]
                    
                    potent   = self.Q[q]*potent
                    fpq      = self.Q[q]*rpqterm     
                    fe[p,:] += fpq
                    Ue      += potent
                    oper    += ndom    
                    cells   += ndom

            fe[p,:] *= self.Q[p]
        
        fe *= ke
        Ue *= ke
                            
        print 'number of algorithmic operations = %d' %(oper) 
        print 'number of theoretical operations for periodic bc= %d' %(self.npart*(self.npart-1)*((2*mesh.nperx+1)**mesh.dper))
        print 'number of theoretical operations non periodic bc= %d' %(self.npart*(self.npart-1))

        return fe, Ue

    def electro_force_extended_particle_mesh(self, mesh):
        "calculates the total electrostatic (Coulomb) force for every particle using the extended particle-mesh algorithm"
        "in the neighbourhood of a particle, periodicity will be taken into account via the periodic images of the particles"

        fe = np.zeros((self.npart, 3)) #resetting forces to zero
        Ue = 0.                        #resetting electric potential to zero
        #Classification of particles per cells and calculation of equivalent charge and position
        self.classify(mesh)
        #counting the number of particles in a cell
#        mesh.cell_count(self)
        #Calculation of equivalent particles
        self.equivalent(mesh)
        #for each particle
        oper1 = 0
        oper2 = 0
        for p in range(self.npart):
            #cell ii,kk,jj contains particle p
            [i, j, k] = self.cell[p]
            c = mesh.cell_number[i,j,k]
            neighbours = mesh.neighbours[c]
            notneighbours = mesh.notneighbours[c]
            
            #for cells that contain particle p or they are neighbors of p's cell (i,j,k)
            for cneigh in neighbours:
                [ii, jj, kk] = mesh.cell_index[cneigh]

                #for every particle q in cell [ii,jj,kk] calculate the particle-to-particle interaction                     
                for q in self.p_in_cells[ii,jj,kk]:
                    if p != q:
                        rpq = self.distance(p, q)
                        rpqterm, potent, ndom = self.electro_dipole(mesh, rpq)
                        
                        #periodicity will be taken into account via the periodic images of the particles in the neighbourhood                                
                        periodicity = self.electro_periodicity(mesh, rpq)
                        rpqterm += periodicity[0]        
                        potent  += periodicity[1]        
                        ndom    += periodicity[2]
                        
                        potent   = self.Q[q]*potent
                        fpq      = self.Q[q]*rpqterm 
                        fe[p,:] += fpq
                        Ue      += potent
                        oper1   += ndom

            #for cells that do not contain particle p nor they are a neighbor of p's cell  
            for cneigh in notneighbours:
                [ii, jj, kk] = mesh.cell_index[cneigh]
                if len(self.p_in_cells[ii,jj,kk])!=0:       
                    #use the equivalent particle to estimate the force between p and cell ii,jj,kk
                    rpk = self.distance_eq(mesh, p, ii, jj, kk)
                    rpkterm, potent, ndom = self.electro_dipole(mesh, rpk)
                    
                    periodicity = self.electro_periodicity(mesh, rpk)
                    rpkterm += periodicity[0]   
                    potent  += periodicity[1]        
                    ndom    += periodicity[2]  
                    
                    potent   = mesh.eqQ[ii,jj,kk]*potent
                    fpk      = mesh.eqQ[ii,jj,kk]*rpkterm                                         
                    fe[p,:] += fpk
                    Ue      += potent
                    oper2   += ndom

            fe[p,:] *= self.Q[p]
        
        fe *= ke
        Ue *= ke

        self.print_operations(mesh, oper1, oper2)

        return fe, Ue

    def electro_force_optimized_particle_mesh(self, mesh):
        "calculates the total electrostatic (Coulomb) force for every particle using the optimized particle-mesh algorithm"
        "in the neighbourhood of a particle, periodicity will be taken into account via the periodic images of the cells that contain the particles"

        fe = np.zeros((self.npart, 3)) #resetting forces to zero
        Ue = 0.                        #resetting electric potential to zero
        #Classification of particles per cells and calculation of equivalent charge and position
        self.classify(mesh)
        #counting the number of particles in a cell
#        mesh.cell_count(self)
        #Calculation of equivalent particles
        self.equivalent(mesh)
        #for each particle
        oper1 = 0
        oper2 = 0
        oper3 = 0
        for p in range(self.npart):
            #cell ii,kk,jj contains particle p
            [i, j, k] = self.cell[p]
            c = mesh.cell_number[i,j,k]
            inneigh  = mesh.inneigh[c]
            notinneigh  = mesh.notinneigh[c]
            
            #for cells that contain particle p or they are neighbors of p's cell (i,j,k)
            iineigh, jjneigh, kkneigh = mesh.neighbours_index(i,j,k)
            for iii in iineigh:
                ii = iii%mesh.nx
                for jjj in jjneigh:
                    jj = jjj%mesh.ny
                    for kkk in kkneigh:
                        #if cell (i,j,k) is a border cell and cneigh is out of the domain                
                        #calculating the periodic copy of the real neighbour cell
                        kk = kkk%mesh.nz
                        if len(self.p_in_cells[ii,jj,kk])!=0:       
                            r = self.redistance(mesh, c, iii, jjj, kkk)
                            
                            #for every particle q in cell [ii,jj,kk] calculate the particle-to-particle interaction
                            for q in self.p_in_cells[ii,jj,kk]:
                                if p != q:
                                    rpq = self.distance(p, q)
                                    rpq += r
                                    rpqterm, potent, ndom = self.electro_dipole(mesh, rpq)
                                    
                                    potent   = self.Q[q]*potent
                                    fpq      = self.Q[q]*rpqterm 
                                    fe[p,:] += fpq
                                    Ue      += potent
                                    oper1   += ndom

            #for cells that do not contain particle p nor they are an in-neighbor of p's cell  
            for cneigh in notinneigh:
                [ii, jj, kk] = mesh.cell_index[cneigh]
                if len(self.p_in_cells[ii,jj,kk])!=0:       
                    #use the equivalent particle to estimate the force between p and cell ii,jj,kk
                    rpk = self.distance_eq(mesh, p, ii, jj, kk)
                    rpkterm, potent, ndom = self.electro_dipole(mesh, rpk)
                    
                    potent   = mesh.eqQ[ii,jj,kk]*potent
                    fpk      = mesh.eqQ[ii,jj,kk]*rpkterm                                         
                    fe[p,:] += fpk
                    Ue      += potent
                    oper2   += ndom
                                
            #periodicity will be taken into account via the periodic images of the cells of the domain
            for cell in range(mesh.ncells):
                [ii, jj, kk]  = mesh.cell_index[cell]                                                   
                if len(self.p_in_cells[ii,jj,kk])!=0:
                    rpk = self.distance_eq(mesh, p, ii, jj, kk)
                    rpkterm, potent, ndom = self.electro_periodicity_pm(mesh, rpk, p, cell)
                    
                    potent   = mesh.eqQ[ii,jj,kk]*potent
                    fpk      = mesh.eqQ[ii,jj,kk]*rpkterm
                    fe[p,:] += fpk
                    Ue      += potent
                    oper3   += ndom
            
            fe[p,:] *= self.Q[p]
        
        fe *= ke
        Ue *= ke

        self.print_operations2(mesh, oper1, oper2, oper3)

        return fe, Ue

    def print_operations(self, mesh, oper1, oper2):
        "print the algorithmic and theoretical number of operations of algorithms extended"
                            
        oper   = oper1+oper2
        print                                            
        print 'number of algorithmic operations = %d '                %(oper) 
        print 'number of theoretical operations for periodic bc= %d'  %(mesh.cells_cost(mesh.nf, self.npart, self.eforce))
        print 'number of theoretical operations non periodic bc= %d ' %(self.npart*(27*mesh.npcell-1 + mesh.nf**3 - 27))

    def print_operations2(self, mesh, oper1, oper2, oper3):
        "print the algorithmic and theoretical number of operations of algorithms optimized"
                            
        oper   = oper1+oper2+oper3        
        print                                            
        print 'number of algorithmic operations = %d '                %(oper) 
        print 'number of theoretical operations for periodic bc= %d'  %(mesh.cells_cost(mesh.nf, self.npart, self.eforce))
        print 'number of theoretical operations non periodic bc= %d ' %(self.npart*(27*mesh.npcell-1 + mesh.nf**3 - 27))
    
        
    def classify(self, mesh):
        "classify each particle to the cell it belongs"
        
        p_in_cells = [ [ [ [] for i in range(mesh.nx) ] for j in range(mesh.ny) ] for k in range(mesh.nz) ] #particles that belong to each cell
        cell = [ [] for i in range(self.npart) ]                                                             #cell in which each particle belongs

        for m in range(self.npart):
            for i in range(mesh.nx):
                for j in range(mesh.ny):
                    for k in range(mesh.nz):
                        #condition for particle m belonging in cell i,j,k
                        if (mesh.xc[i]-mesh.dx/2. < self.xp[m] < mesh.xc[i]+mesh.dx/2.) and \
                           (mesh.yc[j]-mesh.dy/2. < self.yp[m] < mesh.yc[j]+mesh.dy/2.) and \
                           (mesh.zc[k]-mesh.dz/2. < self.zp[m] < mesh.zc[k]+mesh.dz/2.):
                           cell[m] = [i,j,k]
                           p_in_cells[i][j][k].append(m)
                           
        self.cell       = np.array(cell)       #contains the id of the cell for every particle m
        self.p_in_cells = np.array(p_in_cells) #contains the id of particles for every cell i,j,k

    def equivalent(self, mesh):
        "calculate equivalent particle charge and equivalent position"
        
        eqQ  = np.zeros((mesh.nx, mesh.ny, mesh.nz))
        eqxp = np.zeros((mesh.nx, mesh.ny, mesh.nz))
        eqyp = np.zeros((mesh.nx, mesh.ny, mesh.nz))
        eqzp = np.zeros((mesh.nx, mesh.ny, mesh.nz))

        for i in range(mesh.nx):
            for j in range(mesh.ny):
                for k in range(mesh.nz):
                    for m in self.p_in_cells[i,j,k]:
                        eqQ[i,j,k]  += self.Q[m]
                        eqxp[i,j,k] += self.xp[m]*self.Q[m]
                        eqyp[i,j,k] += self.yp[m]*self.Q[m]
                        eqzp[i,j,k] += self.zp[m]*self.Q[m]

        eqQ[eqQ == 0] = np.nan #filling the charge of empty cells with the charge of an electron in order to avoid division by zero     
        mesh.eqxp = eqxp/eqQ
        mesh.eqyp = eqyp/eqQ
        mesh.eqzp = eqzp/eqQ
        mesh.eqQ  = eqQ

        return mesh.eqQ, mesh.eqxp, mesh.eqyp, mesh.eqzp

    def electro_dipole(self, mesh, rpq):
        "calculates the electric potential and distance term for two charged particlesp p and q"
                
        #the distance term for the electrostatic force calculation
        rpqterm = self.unitary(rpq)/np.linalg.norm(rpq)**2
        #the distance term for the electric potential
        potent = 1. / np.linalg.norm(rpq)
        ndom = 1
        
        return rpqterm, potent, ndom

    def distance(self, p, q):
        "calculates the vector of the distance between particle p and q"
        
        rpq = np.empty(3)
        rpq[0] = self.xp[p] - self.xp[q]
        rpq[1] = self.yp[p] - self.yp[q]
        rpq[2] = self.zp[p] - self.zp[q]
        
        return rpq

    def distance_eq(self, mesh, p, i, j, k):
        "calculates the vector of the distance between particle i and cell [i,j,k]"
        
        rpk = np.empty(3)
        rpk[0] = self.xp[p] - mesh.eqxp[i,j,k]
        rpk[1] = self.yp[p] - mesh.eqyp[i,j,k]
        rpk[2] = self.zp[p] - mesh.eqzp[i,j,k]
        
        return rpk

    def redistance(self, mesh, c, iii, jjj, kkk):
        "calculates the real distance vector between a (real) neighbour cell [iii,jjj,kkk] of a cell c and the former's periodic copy"

        [i, j, k]  = mesh.cell_index[c]
        
        il = 0
        jl = 0
        kl = 0
        if (iii < 0 or iii > mesh.nx-1):
            il = np.sign(i-iii)
        if (jjj < 0 or jjj > mesh.ny-1):
            jl = np.sign(j-jjj)                
        if (kkk < 0 or kkk > mesh.nz-1):
            kl = np.sign(k-kkk)

        r = np.array([il*mesh.lx, jl*mesh.ly, kl*mesh.lz])
            
        return r

    def electro_periodicity(self, mesh, rpq):
        "calculates the distance (vector) term and electric potential between particle p and all the periodic clones of q"

        #the distance term for the electrostatic force calculation
        rpqterm = 0.
        #the distance term for the electric potential
        potent = 0.
        #number of domain copies
        ndom = 0
        
        #contribution of periodicity only in x dimension
        if mesh.bcx == 0:
            xper = range(-mesh.nperx,0,1) + range(1,mesh.nperx+1,1)
            for i in xper:
                x = np.array([i*mesh.lx, 0., 0.])
                r = rpq - x
                dipole = self.electro_dipole(mesh, r)
                rpqterm += dipole[0]
                potent  += dipole[1]
                ndom    += dipole[2]

        #contribution of periodicity only in y dimension
        if mesh.bcy == 0:
            yper = range(-mesh.npery,0,1) + range(1,mesh.npery+1,1)
            for j in yper:
                y = np.array([0., j*mesh.ly, 0.])
                r = rpq - y
                dipole = self.electro_dipole(mesh, r)
                rpqterm += dipole[0]
                potent  += dipole[1]
                ndom    += dipole[2]

        #contribution of periodicity only in z dimension
        if mesh.bcz == 0:
            zper = range(-mesh.nperz,0,1) + range(1,mesh.nperz+1,1)
            for k in zper:
                z = np.array([0., 0., k*mesh.lz])
                r = rpq - z
                dipole = self.electro_dipole(mesh, r)
                rpqterm += dipole[0]
                potent  += dipole[1]
                ndom    += dipole[2]
                
        #contribution of periodicity in xy dimensions
        if mesh.bcx == 0 and mesh.bcy == 0:
            for i in xper:
                for j in yper:
                    xy = np.array([i*mesh.lx, j*mesh.ly, 0.])
                    r = rpq - xy
                    dipole = self.electro_dipole(mesh, r)
                    rpqterm += dipole[0]
                    potent  += dipole[1]
                    ndom    += dipole[2]
                    
        #contribution of periodicity in xz dimension
        if mesh.bcx == 0 and mesh.bcz == 0:
            for i in xper:
                for k in zper:
                    xz = np.array([i*mesh.lx, 0., k*mesh.lz])
                    r = rpq - xz
                    dipole = self.electro_dipole(mesh, r)
                    rpqterm += dipole[0]
                    potent  += dipole[1]
                    ndom    += dipole[2]
                    
        #contribution of periodicity in yz dimension
        if mesh.bcy == 0 and mesh.bcz == 0:
            for j in yper:
                for k in zper:
                    yz = np.array([0., j*mesh.ly, k*mesh.lz])
                    r = rpq - yz
                    dipole = self.electro_dipole(mesh, r)
                    rpqterm += dipole[0]
                    potent  += dipole[1]
                    ndom    += dipole[2]

        #contribution of periodicity in xyz dimension
        if mesh.bcx == 0 and mesh.bcy == 0 and mesh.bcz == 0:
            for i in xper:
                for j in yper:
                    for k in zper:
                        xyz = np.array([i*mesh.lx, j*mesh.ly, k*mesh.lz])
                        r = rpq - xyz
                        dipole = self.electro_dipole(mesh, r)
                        rpqterm += dipole[0]
                        potent  += dipole[1]
                        ndom    += dipole[2]
#                        print ndom,'periodicity in xyz',xyz

#        print 'theoretical number of clones due to periodicity: %d' %((2*mesh.nperx+1)**mesh.dper-1)
                                
        return rpqterm, potent, ndom

    def electro_periodicity_pm(self, mesh, rpk, p, cell):
        "calculates the distance (vector) term and electric potential between particle p and all the periodic clones of cell"
        "except for those that fall into the neighbourhood of particle p, as they have been already taken into account by the direct part"

        #the distance term for the electrostatic force calculation
        rpkterm = 0.
        #the distance term for the electric potential
        potent = 0.
        #number of domain copies
        ndom = 0
        
        [i, j, k] = self.cell[p]
        c = mesh.cell_number[i,j,k]
        neigh = mesh.neighbourhood[c]
        [ii, jj, kk]  = mesh.cell_index[cell]                                                   
        dp = np.array([self.xp[p],self.yp[p],self.zp[p]])
        
        #contribution of periodicity only in x dimension
        if mesh.bcx == 0:
            xper = range(-mesh.nperx,0,1) + range(1,mesh.nperx+1,1)
            for i in xper:
                x = np.array([i*mesh.lx, 0., 0.])
                r = rpk - x
                pos = dp - r
                outofneigh = self.outofdomain(pos[0], pos[1], pos[2], neigh)
                if np.any(outofneigh):
                    dipole = self.electro_dipole(mesh, r)
                    rpkterm += dipole[0]
                    potent  += dipole[1]
                    ndom    += dipole[2]

        #contribution of periodicity only in y dimension
        if mesh.bcy == 0:
            yper = range(-mesh.npery,0,1) + range(1,mesh.npery+1,1)
            for j in yper:
                y = np.array([0., j*mesh.ly, 0.])
                r = rpk - y
                pos = dp - r
                outofneigh = self.outofdomain(pos[0], pos[1], pos[2], neigh)
                if np.any(outofneigh):
                    dipole = self.electro_dipole(mesh, r)
                    rpkterm += dipole[0]
                    potent  += dipole[1]
                    ndom    += dipole[2]

        #contribution of periodicity only in z dimension
        if mesh.bcz == 0:
            zper = range(-mesh.nperz,0,1) + range(1,mesh.nperz+1,1)
            for k in zper:
                z = np.array([0., 0., k*mesh.lz])
                r = rpk - z
                pos = dp - r
                outofneigh = self.outofdomain(pos[0], pos[1], pos[2], neigh)
                if np.any(outofneigh):
                    dipole = self.electro_dipole(mesh, r)
                    rpkterm += dipole[0]
                    potent  += dipole[1]
                    ndom    += dipole[2]
                
        #contribution of periodicity in xy dimensions
        if mesh.bcx == 0 and mesh.bcy == 0:
            for i in xper:
                for j in yper:
                    xy = np.array([i*mesh.lx, j*mesh.ly, 0.])
                    r = rpk - xy
                    pos = dp - r
                    outofneigh = self.outofdomain(pos[0], pos[1], pos[2], neigh)
                    if np.any(outofneigh):
                        dipole = self.electro_dipole(mesh, r)
                        rpkterm += dipole[0]
                        potent  += dipole[1]
                        ndom    += dipole[2]
                    
        #contribution of periodicity in xz dimension
        if mesh.bcx == 0 and mesh.bcz == 0:
            for i in xper:
                for k in zper:
                    xz = np.array([i*mesh.lx, 0., k*mesh.lz])
                    r = rpk - xz
                    pos = dp - r
                    outofneigh = self.outofdomain(pos[0], pos[1], pos[2], neigh)
                    if np.any(outofneigh):
                        dipole = self.electro_dipole(mesh, r)
                        rpkterm += dipole[0]
                        potent  += dipole[1]
                        ndom    += dipole[2]
                    
        #contribution of periodicity in yz dimension
        if mesh.bcy == 0 and mesh.bcz == 0:
            for j in yper:
                for k in zper:
                    yz = np.array([0., j*mesh.ly, k*mesh.lz])
                    r = rpk - yz
                    pos = dp - r
                    outofneigh = self.outofdomain(pos[0], pos[1], pos[2], neigh)
                    if np.any(outofneigh):
                        dipole = self.electro_dipole(mesh, r)
                        rpkterm += dipole[0]
                        potent  += dipole[1]
                        ndom    += dipole[2]

        #contribution of periodicity in xyz dimension
        if mesh.bcx == 0 and mesh.bcy == 0 and mesh.bcz == 0:
            for i in xper:
                for j in yper:
                    for k in zper:
                        xyz = np.array([i*mesh.lx, j*mesh.ly, k*mesh.lz])
                        r = rpk - xyz
                        pos = dp - r
                        outofneigh = self.outofdomain(pos[0], pos[1], pos[2], neigh)
                        if np.any(outofneigh):
                            dipole = self.electro_dipole(mesh, r)
                            rpkterm += dipole[0]
                            potent  += dipole[1]
                            ndom    += dipole[2]
                                
        return rpkterm, potent, ndom

    def outofdomain(self, xp, yp, zp, borders):
        "estimates if particle i is out of the domain"

        outofbounds = np.empty(3)

        xmin = borders[0,0] ; xmax = borders[0,1]
        ymin = borders[1,0] ; ymax = borders[1,1]
        zmin = borders[2,0] ; zmax = borders[2,1]

        outofbounds[0] = xp > xmax or xp < xmin  #out of x bounds
        outofbounds[1] = yp > ymax or yp < ymin  #out of y bounds
        outofbounds[2] = zp > zmax or zp < zmin  #out of z bounds
                
        return outofbounds
        
    def superposed(self, i, j):
        "estimates if particle i is superposed on particle j"
        
        self.superposition[i,j,0] = ( ((self.xp[i]+self.r[i] >= self.xp[j]-self.r[j]-self.clearance) and (self.xp[j] >= self.xp[i])) \
        or ((self.xp[j]+self.r[j] >= self.xp[i]-self.r[i]-self.clearance) and (self.xp[i] >= self.xp[j])) )

        self.superposition[i,j,1] = ( ((self.yp[i]+self.r[i] >= self.yp[j]-self.r[j]-self.clearance) and (self.yp[j] >= self.yp[i])) \
        or ((self.yp[j]+self.r[j] >= self.yp[i]-self.r[i]-self.clearance) and (self.yp[i] >= self.yp[j])) ) 

        self.superposition[i,j,2] = ( ((self.zp[i]+self.r[i] >= self.zp[j]-self.r[j]-self.clearance) and (self.zp[j] >= self.zp[i])) \
        or ((self.zp[j]+self.r[j] >= self.zp[i]-self.r[i]-self.clearance) and (self.zp[i] >= self.zp[j])) ) 

    def unitary(self,v):
        "calculates the unitary vector of a vector"
        
        if np.any(v):
            uv = v/np.linalg.norm(v)
            return uv
        else:
            print "cannot define unitary of zero vector"
            return v
    
    def write_distrib(self, mesh, inout, t, it):
        "saves the position, radius and charge of all particles in a file for every time step"

        inout.write_particles(self, mesh, t, it)