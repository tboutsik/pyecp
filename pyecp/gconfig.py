# -*- coding: utf-8 -*-
"""
Created on Thu Oct 26 11:54:15 2017

@author: aboutsik
"""
import time

global ke, current_time, eps, gamma

current_time = time.clock()             #current cpu time
ke           = 8.9875517873681764*10**9 #Coulomb's constant
eps          = 10**(-16)                #machine precision
gamma        = 0.5772156649             #Euler-Mascheroni constant
#it           = 0                        #global time iteration
#t            = 0.                       #global physical time                      