# -*- coding: utf-8 -*-
"""
Created on Mon Oct 16 16:36:35 2017

@author: a.boutsik
"""
from optparse import OptionParser
import time
import sys
import math
import numpy as np

import gconfig
import pyecp

class domain3D():
    "class for a computational domain and its mesh"

    global ke, eps
    ke  = gconfig.ke
    eps = gconfig.eps
    
    def __init__(self, meshtype, p, borders, boundary, epsilon, ncelld, nf):

        #domain borders
        self.borders = borders
        self.xmin = borders[0,0] ; self.xmax = borders[0,1]
        self.ymin = borders[1,0] ; self.ymax = borders[1,1]
        self.zmin = borders[2,0] ; self.zmax = borders[2,1]

        #domain length x, y, z direction
        self.lx = self.xmax - self.xmin
        self.ly = self.ymax - self.ymin
        self.lz = self.zmax - self.zmin
        self.epsilon = epsilon*p.diameter #distance for virtual particles domain
        
        #boundary conditions
        self.boundary = boundary
        self.bcx = boundary[0]
        self.bcy = boundary[1]
        self.bcz = boundary[2]
        self.dper = 0          #number of periodic dimensions
        for i in range(3):
            if boundary[i] == 0:
                self.dper += 1

        #if at least one boundary condition is periodic
        if not np.all(boundary): self.critical_electro_distance(p)

        #mesh setup
        if p.sdistrib == 'centered':
            meshtype = 'manual'

        self.meshtype = meshtype       #mesh creation type
        if self.meshtype == 'manual': 
            self.nf = nf               #if the meshtype is manual set number of cells in x, y, z direction for manual             
        self.ncelld = ncelld           #cell size -to- particle diameter ratio  
        self.mesh_setup(p)             #computational domain discretization set-up 
        
        #enumerating and finding cell neighbours
        self.cell_number, self.cell_index, self.border_cells = self.cell_enumerate()  #definition of ascending cell numbers and border cells

        if p.eforce == 'extended':
            self.neighbours = self.all_neighbours()  #finding all cell neighbours
            self.notneighbours = [ [c for c in range(self.ncells) if c not in self.neighbours[cell]] for cell in range(self.ncells)]
        
        elif p.eforce == 'optimized':
            self.inneigh, self.neighbourhood = self.all_neighbours2()   #finding all cell neighbours
            self.notinneigh = [ [c for c in range(self.ncells) if c not in self.inneigh[cell]] for cell in range(self.ncells) ]

        if p.eforce != 'direct':

            #equivalent particle charge and position for every cell
            self.eqQ  = np.zeros((self.nx, self.ny, self.nz))
            self.eqxp = np.zeros((self.nx, self.ny, self.nz))
            self.eqyp = np.zeros((self.nx, self.ny, self.nz))
            self.eqzp = np.zeros((self.nx, self.ny, self.nz))

        print '----------------------------------------------------------'
#        print "3D computational domain dimensions: %2.4f x %2.4f x %2.4f m^3" %(self.lx, self.ly, self.lz)
        print "3D computational domain borders:   [%2.4f, %2.4f] x [%2.4f, %2.4f] x [%2.4f, %2.4f] m^3" %(self.xmin, self.xmax, self.ymin, self.ymax, self.zmin, self.zmax)
        print "Discretization in %d x %d x %d = %d cells with size = %2.2e m" %(self.nx, self.ny, self.nz, self.ncells, self.dx)

        #calculating local time interval for mesh creation
        tinterval = time.clock() - gconfig.current_time
        gconfig.current_time = time.clock()
        print '----------------------------------------------------------'
        print "Cpu time = %.5f s" %(tinterval)
        print 'To create %s mesh with %d cells' %(self.meshtype, self.ncells)
        print '----------------------------------------------------------'


    def cell_enumerate(self):
        "enumerate each cell, create a map such that (i,j,k)--> c"
        
        cell_number = np.empty((self.nx, self.ny, self.nz), dtype=int) #superposition with other particles
        cell_index  = [ [] for i in range(self.ncells) ]
        border_cells = []
        
        counter = -1
        
        for i in range(self.nx):
            for j in range(self.ny):
                for k in range(self.nz):
                                           
                   counter += 1
                   cell_number[i,j,k] = counter
                   cell_index[counter] = [i,j,k]

        for cell in range(self.ncells):
            [i,j,k] = cell_index[cell]
            if ((i == 0 or i==self.nx-1) or (j == 0 or j==self.ny-1) or (k == 0 or k==self.nz-1)):
                border_cells.append(cell)
#                print 'cell ',[i,j,k],' number ',cell, 'with nodes: ', [ [self.xf[i], self.xf[i+1]], [self.yf[j], self.yf[j+1]], [self.zf[k], self.zf[k+1]] ], 'is a border cell'

        nx = self.nx -1  
        ny = self.ny -1        
        nz = self.nz -1        
        length = 2*((nx-1)*(ny-1) + (nx-1)*(nz-1) + (ny-1)*(nz-1)) + 4*(nx + ny + nz -1)
        if len(border_cells) != length:
            raw_input('there is a BUG in the code!!!')
                                           
        return cell_number, cell_index, border_cells
    
    def critical_electro_distance(self, p):
        "calculates the critical electrostatic interaction distance"

        if p.eforce == 'direct':
            self.rc = np.sqrt(ke/eps)*p.icharge
        else:
#            self.rc     = np.sqrt(self.npcell*ke/eps)*p.icharge   #critical electrostatic distance
            self.rc = np.sqrt(ke/eps)*p.icharge
            
        self.nperx = int(np.ceil(self.rc/self.lx)) - 1     #number of domains to span to cover rc
        self.npery = int(np.ceil(self.rc/self.ly)) - 1     #number of domains to span to cover rc
        self.nperz = int(np.ceil(self.rc/self.lz)) - 1     #number of domains to span to cover rc

        print 'critical electrostatic interaction distance: %2.5f' %self.rc
        print 'domain periodic span: %2d, %2d, %2d' %(self.nperx, self.npery, self.nperz)

    def mesh_setup(self, p):
        "performs the discretization of the computational domain and creates the cell nodes and centers"

        [nx, ny, nz, dx, dy, dz] = self.discretization(p)
        
        #number of cells in x, y, z direction                
        self.nx = nx
        self.ny = ny
        self.nz = nz
        self.ncells = nx*ny*nz
        self.npcell = float(p.npart)/float(self.ncells)       #number of particles per cell
        print 'average number of particles per cell: %2.4f' %self.npcell

        #cell size in x, y, z direction                
        self.dx = dx
        self.dy = dy
        self.dz = dz

        [self.xf, self.yf, self.zf]= self.nodes()        
        [self.xc, self.yc, self.zc]= self.centers()

        self.le = max(self.lx, self.ly, self.lz)  #characteristic length of Coulomb's interaction    
        self.volume = self.lx*self.ly*self.lz     #computational domain volume
    
    def discretization(self, p):
        "calculates number of cells and cell size for a given domain and mesh type"
        
        if self.meshtype == "eq_particle":
            #number of cells for particle-mesh algorithm
            self.nf = nx = ny = nz = self.cells_optimal(p.npart, p.eforce)
            
            dx = self.lx/nx
            dy = self.ly/ny
            dz = self.lz/nz

            print 'Creating mesh of type %s with optimal number of cells per dimension = %d' %(self.meshtype, self.nf)
            print
            
        elif self.meshtype == "poisson":
            dx = dy = dz = self.ncelld*p.diameter #cell size as a multiple of particle diameter
            nx = int(math.floor(self.lx/dx))
            ny = int(math.floor(self.ly/dy))
            nz = int(math.floor(self.lz/dz))
            self.nf = nx

        elif self.meshtype == "manual":
            nx = ny = nz = self.nf
            print 'Creating mesh of type %s with forced number of cells per dimension = %d' %(self.meshtype, self.nf)
            dx = self.lx/nx
            dy = self.ly/ny
            dz = self.lz/nz
                
        return nx, ny, nz, dx, dy, dz

    def cells_optimal(self, n, eforce):
        
        if eforce == 'optimized':
            per = (2*self.nperx+1)**self.dper
            nf = np.sqrt(3)*(float(n)/float(per))**(1./6.)
            
        else: #elif p.eforce == 'extended' or p.eforce == 'direct':
            nf = np.sqrt(3)*n**(1./6.)
    
        nceil  = int(math.ceil(nf))
        nfloor = int(math.floor(nf))
        #cost of particle-mesh algorithm
        cceil  = self.cells_cost(nceil,  n, eforce)   
        cfloor = self.cells_cost(nfloor, n, eforce)
        
        if cceil < cfloor: #choosing the integer that minimizes the computational cost
            nf = nceil
        else: #cceil <= cfloor
            nf = nfloor
        
        #checking if number of cells is equal to 1 and imposing it equal to 2
        if nf == 1:
            nf = 2    

        print '=========================================================='
        print '%2d cells cost = %d' %(nceil,  cceil)
        print '%2d cells cost = %d' %(nfloor, cfloor)
        print '=========================================================='
        
        return nf

    def cells_cost(self, nf, n, eforce):
        "calculates the computational cost for extended and optimized particle-mesh algorithms"
        "n : number of particles"
        "nf: number of mesh cells in one direction"
        "per: total number of domain periodic copies" 

        per = (2*self.nperx+1)**self.dper    
        npc = float(n)/float(nf**3)
        
        if eforce == 'optimized':
            cost = n*(27*npc-1 + per*nf**3 - 27)
            
        else: #elif p.eforce == 'extended' or p.eforce == 'direct':
            cost = n*(27*npc-1 + nf**3 - 27)*per
        
        return cost

    def cartesian_check(self):
        "checks if mesh is cartesian"
        
        if not (self.dx == self.dy == self.dz):
            print "The mesh is not cartesian", self.dx, self.dy, self.dz
                
    #mesh creation (nodes)
    def nodes(self):
        "compute nodes of cells in a 3D mesh"
        xf = np.linspace(self.xmin, self.xmax, self.nx+1)
        yf = np.linspace(self.ymin, self.ymax, self.ny+1)
        zf = np.linspace(self.zmin, self.zmax, self.nz+1)
        
        return xf, yf, zf
        
    #mesh creation (centers)
    def centers(self):
        "compute centers of cells in a mesh"
        xc = np.zeros(self.nx)
        yc = np.zeros(self.ny)
        zc = np.zeros(self.nz)
                
        for i in range(self.nx):
            xc[i] = (self.xf[i]+self.xf[i+1])/2.
            
        for j in range(self.ny):
            yc[j] = (self.yf[j]+self.yf[j+1])/2.
            
        for k in range(self.nz):
            zc[k] = (self.zf[k]+self.zf[k+1])/2.
            
        return xc, yc, zc

    def all_neighbours(self):
        "returns all neighbours for each cell"

        neighbours    = [ [] for i in range(self.ncells) ]
        
        for cell in range(self.ncells):
            [i,j,k] = self.cell_index[cell]      
            
            iineigh, jjneigh, kkneigh = self.neighbours_index(i,j,k)
                
            for iii in iineigh:
                for jjj in jjneigh:
                    for kkk in kkneigh:
                        #if cell (i,j,k) is a border cell and cell [iii,jjj,kkk] is out of the domain
                        if (iii < 0 or iii > self.nx-1) or (jjj < 0 or jjj > self.ny-1) or (kkk < 0 or kkk > self.nz-1):
                            ii = iii%self.nx 
                            jj = jjj%self.ny 
                            kk = kkk%self.nz
                            cneigh = self.cell_number[ii, jj, kk]
                        else:
                            cneigh = self.cell_number[iii, jjj, kkk]
                            
                        if cneigh not in neighbours[cell]:
                            neighbours[cell].append(cneigh)
                            
            neighbours[cell].sort()

        return neighbours

    def all_neighbours2(self):
        "returns in-neighbours and neighbourhood for each cell"

        inneigh       = [ [] for i in range(self.ncells) ]
        neighbourhood = [ [] for i in range(self.ncells) ]
        
        for cell in range(self.ncells):
            neigh = np.empty((3,2))                   #neighbourhood space limits for every cell
            [i,j,k] = self.cell_index[cell]
            neigh[0,0] = self.xf[i] - self.dx ; neigh[0,1] = self.xf[i+1] + self.dx
            neigh[1,0] = self.yf[j] - self.dy ; neigh[1,1] = self.yf[j+1] + self.dy
            neigh[2,0] = self.zf[k] - self.dz ; neigh[2,1] = self.zf[k+1] + self.dz
            neighbourhood[cell] = neigh              
            
            iineigh, jjneigh, kkneigh = self.neighbours_index(i,j,k)
                
            for iii in iineigh:
                for jjj in jjneigh:
                    for kkk in kkneigh:
                        #if cell (i,j,k) is a border cell and cell [iii,jjj,kkk] is out of the domain
                        if (iii >= 0 and iii <= self.nx-1) and (jjj >= 0 and jjj <= self.ny-1) and (kkk >= 0 and kkk <= self.nz-1):
                            
                            cneigh = self.cell_number[iii, jjj, kkk]
                            if cneigh not in inneigh[cell]:
                                inneigh[cell].append(cneigh)

            inneigh[cell].sort()
                                
        return inneigh, neighbourhood

    def neighbours_index(self, i, j, k):
        "returns the interval of neighbours indices for a cell i, j, k (including itself)"

        #x direction
        iistart = i-1
        iiend = i+2
        #check if the cell is on the borders of x dimension
        if self.bcx == 1:
            if i == 0:
                iistart = i
            elif i == self.nx - 1:
                iiend = i+1
            iineigh = np.arange(iistart ,iiend, 1)
        elif self.bcx == 0:
            iineigh = np.arange(iistart ,iiend, 1)

        #y direction
        jjstart = j-1
        jjend = j+2
        #check if the cell is on the borders of y dimension
        if self.bcy == 1:
            if j == 0:
                jjstart = j
            elif j == self.ny - 1:
                jjend = j+1
            jjneigh = np.arange(jjstart ,jjend, 1)
        elif self.bcy == 0:
            jjneigh = np.arange(jjstart ,jjend, 1)

        #z direction
        kkstart = k-1
        kkend = k+2
        #check if the cell is on the borders of z dimension
        if self.bcz == 1:
            if k == 0:
                kkstart = k
            elif k == self.nz - 1:
                kkend = k+1
            kkneigh = np.arange(kkstart ,kkend, 1)
        elif self.bcz == 0:
            kkneigh = np.arange(kkstart ,kkend, 1)
                
        return iineigh, jjneigh, kkneigh

    def cell_count(self, p):
        "counts the number of particles in all cells i,j,k"

        self.pcount = np.zeros((self.nx, self.ny, self.nz)) #number of particles in a cell
#        self.pcount = np.array([len(p.p_in_cells[i,j,k]) for i in range(self.nx) for j in range(self.ny) for k in range(self.nz)])

        for i in range(self.nx):
            for j in range(self.ny):
                for k in range(self.nz):
                    self.pcount[i,j,k] =  len(p.p_in_cells[i,j,k]) 
#                    if self.pcount[i,j,k] == 0: print 'cell', i, j, k, 'has no particles'
        if np.sum(self.pcount) != p.npart: raw_input('PARTICLES GONE MISSING! THERE IS A BUG IN THE CODE!')
        
    def write_cells(self, inout, particles, t, it):
        "saves the number of particles, equivalent charge and position of all cells in a file for every time step"
        
        inout.write_mesh(self, particles, t, it)

class virtual(domain3D):
    "class of virtual computational domain which is a subdomain of the actual user-defined domain. Its borders have a user-defined offset from the original ones"

    def __init__(self, m):

        self.xmin = m.borders[0][0] ; self.xmax = m.borders[0][1]
        self.ymin = m.borders[1][0] ; self.ymax = m.borders[1][1]
        self.zmin = m.borders[2][0] ; self.zmax = m.borders[2][1]

        self.xmin += m.epsilon
        self.ymin += m.epsilon
        self.zmin += m.epsilon
        
        self.xmax -= m.epsilon
        self.zmax -= m.epsilon
        self.ymax -= m.epsilon
        
        self.lx = self.xmax - self.xmin
        self.ly = self.ymax - self.ymin
        self.lz = self.zmax - self.zmin
        
        print "Virtual domain dimensions: %2.4f x %2.4f x %2.4f m^3" %(self.lx, self.ly, self.lz)