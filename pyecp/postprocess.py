# -*- coding: utf-8 -*-
"""
Created on Thu Nov 16 15:49:07 2017

@author: aboutsik
"""
import numpy as np
import gconfig

class postprocess():
    "class that manages the pyecp code"

    global ke, gamma, eps
    ke  = gconfig.ke
    gamma = gconfig.gamma
    eps = gconfig.eps

    def __init__(self, p, mesh):
        "particles class attributes initialization"
        
        #post-processing quantities
        N = p.npart
        L = mesh.le
        H = np.log((N-1)/2.) + gamma +1./(N-1)
        self.Uemin = N*(N+1)*ke*p.icharge**2/L * H       
        self.Ue  = 0. #electric  potential energy 
        self.Ek  = 0. #particles kinetic   energy
        self.Et  = 0. #particles total     energy
        self.Rp  = np.full((3, 3), np.nan) #auto-correlation tensor
        self.rp  = 1./3.*np.sum(self.Rp)   #auto-correlation function
        self.qp2 = np.nan                  #particle agitation
        self.stable = False                #system stability boolean
        self.told   = 0.                   #keeping time

    def energy(self, p, it, t):
        "post-processing of particles electrostatic interaction"
        
        #when the mean ensemble of forces on the particles is null, the system is considered stable
        fen = np.array([np.linalg.norm(p.fe[i,:]) for i in range(p.npart)]) #norms of all ve vectors
        fdn = np.array([np.linalg.norm(p.fd[i,:]) for i in range(p.npart)]) #norms of all ve vectors
        ferror = self.relative_error(np.mean(fdn), np.mean(fen))
        
        print 'Electrostatic-drag relative difference: ' + '{:.2%}'.format(ferror)
        
        #calculating the temporal derivative of electric potential
        if it!=0:
            Uedt = abs(0.5*p.Ue-self.Ue)/(t-self.told)
            print 'temporal derivative of electric potential: %2.4e' %Uedt

            #when the electric potential reaches a plateau        
#            if Uedt < eps or self.stable:  
            if t > p.tpe or self.stable:       
                if not self.stable:
                    p.vei = p.ve  #initial velocity vectors for each particle
                    self.stable = True
                 
                print '*************SYSTEM CONSIDERED STABLE*************'
                self.rp  = self.auto_correlation(p) #autocorrelation function
                self.qp2 = self.agitation(p)        #particle agitation
    
        #calculating electric, kinetic and total energy of the particles
        self.Ue = 0.5*p.Ue        #electric  potential energy
        self.Ek = p.Ek            #particles kinetic   energy
        self.Et = self.Ue+self.Ek #particles total     energy

        #keeping time
        self.told = t
        
        return self.qp2     
    
    def auto_correlation(self, p):
        "auto-correlation function of particle velocity"
        
        for i in range(3):
            for j in range(3):
                
                numer = p.vei[:,i]*p.ve[:,j]
                denom = p.vei[:,i]*p.vei[:,j]
                self.Rp[i,j] = self.ensemble_avg(p, numer)/self.ensemble_avg(p, denom)
                
        rp = 1./3.*np.trace(self.Rp) #trace of autocorrelation tensor
        print 'Autocorrelation tensor diagonal elements: %2.5f, %2.5f, %2.5f' %(self.Rp[0,0], self.Rp[1,1], self.Rp[2,2])

        return rp  

    def ensemble_avg(self, particles, f):
        "calculates the ensemble average of quantity f"
        
        me = 0.
        for p in range(particles.npart):
            me += f[p]
        me = me/particles.npart
        
        return me

    def agitation(self, p):
        "calculates particle agitation"
        
        qp2 = 0.5*np.mean(p.ven**2)

        return qp2   
    
    def relative_error(self, q, qref):
        "calculates the relative error between a calculated quantity q and its reference value qref"
        
        error = np.abs((qref - q)/qref)
        
        return error
    
    def electric_potential_energy(self, p):
        "calculates the electric potential energy of all particles in the domain"
        
        si = 0.
        for i in range(p.npart):
            sj = 0.
            for j in range(p.npart):
                if j != i:
                    rij = p.distance(i, j)
                    sj += p.Q[j] / np.linalg.norm(rij)
            si += p.Q[i]*sj
        Ue = 0.5*ke*si
        
        return Ue
        

    def write_postprocess(self, inout, particles, t, it):
        "saves the total, kinetic and potential energy of all particles per time step"

        inout.write_energy(self, particles, t, it)
        inout.write_correlations(self, particles, t, it)