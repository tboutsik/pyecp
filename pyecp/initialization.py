# -*- coding: utf-8 -*-
"""
Created on Mon Oct 16 16:36:35 2017

@author: a.boutsik
"""
import numpy as np
import random
import time

import gconfig

class initialization():
    "initial particle position, radius and/or velocity"

    global ke
    ke = gconfig.ke

    def __init__(self, p):

        if p.sdistrib == 'file':
            outdir = '../cases/'
            initname = 'init_' + str(p.npart) + '_' + ("mono" if p.dispersion == 0 else "poly") + '.txt'
            self.file_in = outdir + initname
        
    def distrib(self, virtualmesh, mesh, p):
        "initial distribution (position, radius, velocity) of particles"

        #checking maximum diameter
        self.diameter_check(mesh, p)

        #particles diameter distribution
        p.r, p.mp = self.diameter_distrib(p)

        #particles initial velocity
        p.up, p.vp, p.wp = self.vel_distrib(mesh, p)

        #particles positioning    
        if p.sdistrib == 'uniform': #all particles at once
             p.xp, p.yp, p.zp = self.uniform_distrib(virtualmesh, p)   
             
        elif p.sdistrib == 'random':
            self.random_distrib(virtualmesh, p)

        elif p.sdistrib == 'dipole':
            p.xp, p.yp, p.zp = self.dipole(virtualmesh, p)

        elif p.sdistrib == 'centered':
            p.xp, p.yp, p.zp = self.centered_distrib(mesh, p)
            
        elif p.sdistrib == 'file':
            p.xp, p.yp, p.zp = self.file_distrib()

        volume = 0.
        area = 0.
        for i in range(p.npart):
            area   += 4.0 *np.pi * p.r[i]**2
            volume += 4.0/3.0 *np.pi * p.r[i]**3

        print 'total domain volume   = %2.4e m^3' %(mesh.volume)
        print 'total particle volume = %2.4e m^3' %(volume)
        print 'total particle area   = %2.4e m^2' %(area)
        print 'Sauter Mean Diameter  = %2.4e m'   %(6.*volume/area)
        print 'solid fraction        = %2.4f'     %(volume/mesh.volume)
        print '----------------------------------------------------------'

    def diameter_check(self, mesh, p):
        "diameter check: cell size has to be ncellsd times bigger than particle diameter" 
        
        if mesh.ncelld*p.diameter > mesh.dx: 
            print  "The cell size to maximum diameter ratio is too small:", mesh.dx/p.diameter
    
    def diameter_distrib(self, p):
        "diameter allocation for all particles at once"

        if p.sdistrib == 'file':
            
            d = np.genfromtxt(self.file_in, usecols = (6), skip_header=2, unpack=True)
            r = d/2.
                       
        else: #if p.sdistrib != 'file'
            
            r  = np.zeros(p.npart)  #particles radius
    
            if p.dispersion == 0: 
                #mono-disperse distribution of particles
                if p.radius < p.rmin:
                    p.radius += p.rmin
                r[:] = p.radius    
                
            else: #if p.dispersion == 1  
                #poly-disperse distribution of particles
                r = np.random.uniform(low=p.rmin, high=p.radius, size=(p.npart,))

        #particle mass calculation
        mp = 4./3. * p.rho * np.pi * r**3

        return r, mp

    def vel_distrib(self, mesh, p):
        "velocity allocation for all particles at once"
        
        if p.vdistrib == 'random':
            ue = np.sqrt(p.npart*ke*p.icharge**2/(mesh.le*min(p.mp))) #characteristic velocity due to electric interaction
            harvest = np.random.uniform(low=-ue, high=ue, size=(p.npart,3))
            for i in range(p.npart):
                up = harvest[:,0]
                vp = harvest[:,1]
                wp = harvest[:,2]
            
        elif p.vdistrib == 'uniform':
            up = 0.1*np.ones(p.npart)
            vp = 0.1*np.ones(p.npart)
            wp = 0.1*np.ones(p.npart)

        elif p.vdistrib == 'zero':
            up = np.zeros(p.npart)
            vp = np.zeros(p.npart)
            wp = np.zeros(p.npart)

        elif p.vdistrib == 'file':
            up, vp, wp = np.genfromtxt(self.file_in, usecols = (3, 4, 5), skip_header=2, unpack=True)
        
        return up, vp, wp

    def uniform_distrib(self, mesh, particles):
        "uniform initial position of particles"

        a, npx, npy, npz = self.equidistance(mesh, particles)    
        particles.npart = npx*npy*npz
        print 'Number of particles after solving equidistance problem = %d' %(particles.npart)
                
        xp  = np.zeros(particles.npart) #particles x position vector
        yp  = np.zeros(particles.npart) #particles y position vector
        zp  = np.zeros(particles.npart) #particles z position vector
        
        #particles counter
        p = 0
        #position initialization
        init = mesh.xmin - a
        x = init
        y = init
        z = init
        for i in range(npx):
            x += a
            y = init
            z = init
            for j in range(npy):
                y += a
                z = init
                for k in range(npz):
                    z += a
                    xp[p] = x
                    yp[p] = y
                    zp[p] = z
                    p += 1

        return xp, yp, zp
    
    def equidistance(self, mesh, p):
        "solves the particles equidistant placement problem. returns distance and number of particles per direction"        

        lx = mesh.lx
        ly = mesh.ly
        lz = mesh.lz
        
        #solving the polynomial equation of the equidistance problem                
        coeff = np.empty(4)
        coeff[0] = 1.-p.npart 
        coeff[1] = lx+ly+lz
        coeff[2] = lx*ly + lx*lz + ly*lz
        coeff[3] = lx*ly*lz
        roots = np.roots(coeff)
        
        #choosing the real root as the equidistance
        for i in range(len(roots)):
            if not np.imag(roots[i]):
                a = float(np.real(roots[i]))
                
        print 'particles equidistance = %.5e' %(a) 
        
        #number of particles along 3 directions
        npx = int(round((lx + a)/a))
        npy = int(round((ly + a)/a))
        npz = int(round((lz + a)/a))
        
        return a, npx, npy, npz

    def centered_distrib(self, mesh, p):
        "initial position of particles in the center of each cell"
        
        xp = np.empty(p.npart)
        yp = np.empty(p.npart)
        zp = np.empty(p.npart)

        for cell in range(mesh.ncells):
            [i, j, k] = mesh.cell_index[cell]
            xp[cell] = mesh.xc[i]
            yp[cell] = mesh.yc[j]
            zp[cell] = mesh.zc[k]
            
        return xp, yp, zp

    def dipole(self, mesh, p):
        "initial position of particles in the center of each cell"
        
        xp = np.empty(p.npart)
        yp = np.empty(p.npart)
        zp = np.empty(p.npart)

        xp[0] = 0. 
        yp[0] = mesh.ymin + 0.4*mesh.ly 
        zp[0] = 0.

        xp[1] =  xp[0] 
        yp[1] = -yp[0] 
        zp[1] =  zp[0]
            
        return xp, yp, zp

    def file_distrib(self):
        "initial position of particles using a saved file"

        xp, yp, zp = np.genfromtxt(self.file_in, usecols = (0, 1, 2), skip_header=2, unpack=True)
        
        return xp, yp, zp
                
    def random_distrib(self, mesh, p): 
        "random initial position of particles"
         
        #Creation of particle #i
        for i in range(p.npart):

            #initializing position of particle #i (radius is already set)
            p.xp[i], p.yp[i], p.zp[i] = self.random_position(i, mesh, p)
            j = 0
            
            #Comparing with every previous particle
            while j < i:    #exits if j>=i
            
                #checking particle couple (i,j) superposition criteria          
                p.superposed(i,j)
                #checking if the new particle i is superimposed on j
                if np.all(p.superposition[i,j,:]):  

                    self.move_it(i, mesh, p)
                    #resetting particle check at 0 (1st particle)
                    j = 0
                else:
                    
                    #it is not superimposed on particle j so continuing to particle #j+1
                    j += 1

    def random_position(self, i, mesh, p):
        "spatial allocation of particle i"
        
        xp = np.random.uniform(low=mesh.xmin+p.r[i], high=mesh.xmax-p.r[i])
        yp = np.random.uniform(low=mesh.ymin+p.r[i], high=mesh.ymax-p.r[i])
        zp = np.random.uniform(low=mesh.zmin+p.r[i], high=mesh.zmax-p.r[i])
        
        return xp, yp, zp 

    def put_it_back_in(self, i, mesh, p):
        "puts the particles back in the domain"
        
        p.outofbounds[i,0] = ( p.xp[i]+p.r[i] >= mesh.xmax or p.xp[i]-p.r[i] <= mesh.xmin )  #out of x bounds                
        if ( p.outofbounds[i,0] ):
            
            #Out of x bounds
            if (p.xp[i]+p.r[i] >= mesh.xmax):
                p.xp[i] = mesh.xmax - p.r[i] - p.clearance
            elif  ( p.xp[i]-p.r[i] <= mesh.xmin ):
                p.xp[i] = mesh.xmin + p.r[i] + p.clearance

        p.outofbounds[i,1] = ( p.yp[i]+p.r[i] >= mesh.ymax or p.yp[i]-p.r[i] <= mesh.ymin )  #out of y bounds
        if ( p.outofbounds[i,1] ):
            
            #Out of y bounds
            if (p.yp[i]+p.r[i] >= mesh.ymax):
                p.yp[i] = mesh.ymax - p.r[i] - p.clearance
            elif  ( p.yp[i]-p.r[i] <= mesh.ymin ):
                p.yp[i] = mesh.ymin + p.r[i] + p.clearance

        p.outofbounds[i,2] = ( p.zp[i]+p.r[i] >= mesh.zmax or p.zp[i]-p.r[i] <= mesh.zmin )  #out of z bounds
        if ( p.outofbounds[i,2] ):
            
            #Out of z bounds         
            if (p.zp[i]+p.r[i] >= mesh.zmax):
                p.zp[i] = mesh.zmax - p.r[i] - p.clearance
            elif  ( p.zp[i]-p.r[i] <= mesh.zmin ):
                p.zp[i] = mesh.zmin + p.r[i] + p.clearance

    def move_it(self, i, mesh, p):
        "moves the particle to avoid superposition"
        
        indx = np.random.randint(3)
        # re-position of particle #i
        if (indx == 0): 
            p.xp[i] = np.random.uniform(low=mesh.xmin+p.r[i], high=mesh.xmax-p.r[i])
        elif (indx == 1): 
            p.yp[i] = np.random.uniform(low=mesh.ymin+p.r[i], high=mesh.ymax-p.r[i])
        else: #indx == 2 
            p.zp[i] = np.random.uniform(low=mesh.zmin+p.r[i], high=mesh.zmax-p.r[i])     
        
    def charge(self, p):
        "initial particle charge"
        
        p.Q.fill(p.icharge)