# -*- coding: utf-8 -*-
"""
Created on Mon Oct 30 14:12:03 2017

@author: aboutsik
"""
import subprocess
import numpy as np
from pylab import *
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.backends.backend_pdf import PdfPages
from matplotlib.ticker import MaxNLocator
from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes
from mpl_toolkits.axes_grid1.inset_locator import mark_inset
from matplotlib.ticker import ScalarFormatter, FormatStrFormatter

import matplotlib as mpl
axisfontsize = 22
ticksfontsize = 20
mpl.rcParams['xtick.labelsize'] = ticksfontsize
mpl.rcParams['ytick.labelsize'] = ticksfontsize
mpl.rcParams['legend.fontsize'] = ticksfontsize
mpl.rcParams['font.size']       = ticksfontsize
mpl.rcParams['figure.figsize']  = [11, 8]
mpl.rcParams['figure.dpi']      = 100
mpl.rcParams['savefig.dpi']     = 150
mpl.rcParams['text.usetex']     = True
mpl.rcParams['font.family']     = 'serif'

import gconfig

class inout():
    "class for input/output"

    global eps
    eps = gconfig.eps

    def __init__(self, particles, it_step, pl_step, savetxt, drawplot, saveplot, showplot, tracking, vectors):
        
        self.it_step  = it_step  #iteration step for which data is written in .txt files
        self.pl_step  = pl_step  #iteration step for which data is plotted in .png files
        self.savetxt  = savetxt  #option to save the txts                        0: no, 1: yes
        self.drawplot = drawplot #option to create the plots                     0: no, 1: yes
        self.saveplot = saveplot #option to save the plots                       0: no, 1: yes
        self.showplot = showplot #option to show the plot in the terminal        0: no, 1: yes (block=True) 2:yes (block=False)
        self.tracking = tracking #option to plot the trajectory of the particles 0: no, 1: yes
        self.vectors  = vectors  #option to plot the velocity and force vectors  0: no, 1: yes  
        self.suffix = str(particles.npart) + '_' + particles.sdistrib + '_' + ("mono" if particles.dispersion == 0 else "poly") + '_' + particles.eforce

        #Particle tracking initialization
        if self.tracking == 1:
            self.xt = [ [] for i in range(particles.npart) ]
            self.yt = [ [] for i in range(particles.npart) ]
            self.zt = [ [] for i in range(particles.npart) ]
        
    def eplot(self, mesh, particles, postp, it, t):
        "plots the 3d scatter using the correct data and saves the corresponding .png"
            
        # 3D Plot
        xp = particles.xp
        yp = particles.yp
        zp = particles.zp
        dp = 2*particles.r
        
        #undimensionalize the electrostatic force
        fe = particles.fe    
        fen = np.array([ np.linalg.norm(fe[i,:]) for i in range(particles.npart) ])
        femn = np.mean(fen)
        fe = fe/femn

        #undimensionalize the drag force        
        fd = particles.fd
        fdn = np.array([ np.linalg.norm(fd[i,:]) for i in range(particles.npart) ])
        fdmn = np.mean(fdn)
        if fdmn !=0.: fd = fd/fdmn
        
        ve = np.array([particles.up, particles.vp, particles.wp]).transpose()
        
        fig = self.scatter3d(t, mesh, particles, postp, xp, yp, zp, dp, fe, fd, ve, colorsMap='RdYlBu')

        #saving the plot
        if self.saveplot == 1:        
            sit = self.it_to_sit(it)
            outdir = './particles/png/'
            pngname = 'p_' + self.suffix + '_' + sit + ".png"
            plt.savefig(outdir+pngname, bbox_inches=None)
            
        plt.close(fig)

    def scatter3d(self, t, mesh, particles, postp, xp, yp, zp, cs, fe, fd, ve, colorsMap):
        "creates a 3d scatter of points given by x, y, z in a domain defined by borders."
        "colors them according to cs and adds vectors of fe and ve" 
        
        cm = plt.get_cmap(colorsMap)
        fig = plt.figure(figsize=(10,8))
        
        ax = Axes3D(fig)
        ax.view_init(elev=20., azim=35.)
        title = particles.sdistrib + r' particles $d_p$-colored with $\vec{v}$ and $\vec{F}_e$ vectors'
        ax.set_title(title, fontsize=16)
    
        #use borders to as axes limits
        ax.set_xlim(mesh.xmin, mesh.xmax)
        ax.set_ylim(mesh.ymin, mesh.ymax)
        ax.set_zlim(mesh.zmin, mesh.zmax)
        
        #use multiple of cell size as axes ticks
        m = 1
        ax.xaxis.set_ticks(np.arange(mesh.xmin, mesh.xmax+eps, m*mesh.dx))
        ax.yaxis.set_ticks(np.arange(mesh.ymin, mesh.ymax+eps, m*mesh.dy))
        ax.zaxis.set_ticks(np.arange(mesh.zmin, mesh.zmax+eps, m*mesh.dz))
    
#        ax.scatter(xp, yp, zp, c=scalarMap.to_rgba(cs))
        scale = 60*max(mesh.lx, mesh.ly, mesh.lz)/min(cs)
        sc = ax.scatter(xp, yp, zp, c=cs, cmap = cm, s=scale*cs)
        #adding the colour bar of diameters
        fig.colorbar(sc)
        
        ax.set_xlabel(r'$X$ $(m)$', labelpad = 15)
        ax.set_ylabel(r'$Y$ $(m)$', labelpad = 15)
        ax.set_zlabel(r'$Z$ $(m)$', labelpad = 15)
        
        ax.tick_params(axis='both', which='major', pad=10)

        #adding a text line with physical time
        ax.text(0.4*mesh.xmax, -0.3*mesh.ymax, 1.00*mesh.zmax, (r'$t$ $=$ $%3.5f s$' %(t)),                       weight="bold")
        ax.text(0.4*mesh.xmax, -0.3*mesh.ymax, 0.90*mesh.zmax, (r'unstable' if postp.stable == 0 else r'stable'), weight="bold")
 
        #plot 3D field of electrostatic force and velocity vectors for each particle
        if self.vectors == 1:
            lv = 0.05*min(mesh.lx, mesh.ly, mesh.lz) #vector length as 5% of the minimum domain dimension
            ax.quiver(xp, yp, zp, fe[:,0], fe[:,1], fe[:,2], arrow_length_ratio =0.2, length = lv, pivot = 'tail', color='g', linewidths = 0.5)
            ax.quiver(xp, yp, zp, ve[:,0], ve[:,1], ve[:,2], arrow_length_ratio =0.2, length = lv, pivot = 'tail', color='r', linewidths = 0.5)
            if particles.dforce:
                ax.quiver(xp, yp, zp, fd[:,0], fd[:,1], fd[:,2], arrow_length_ratio =0.2, length = lv, pivot = 'tail', color='b', linewidths = 0.5)

        #particle tracking
        if self.tracking == 1: self.track(mesh, particles, ax)
        
        #show the plot
        if self.showplot == 1:
            plt.show()
            
        elif self.showplot == 2: 
            plt.draw()
            plt.show(block=False)
            plt.pause(0.5)
            
        return fig

    def track(self, mesh, particles, ax):
        "plots the trajectory line for all particles"
        
        maxl = 0.8 #maximum length of displacement for which trajectory is plotted as a continuous line as a percentage of domain dimensions 
        
        xt = self.xt
        yt = self.yt
        zt = self.zt
        
        for p in range(particles.npart):
            xt[p].append(particles.xp[p])
            yt[p].append(particles.yp[p])
            zt[p].append(particles.zp[p])
            
            #if particle p has advanced at least two time steps since it entered the domain
            if len(xt[p]) > 1:        
                #for each such time step i
                for i in range(len(xt[p])-1):
                    #if particle p gets back in the periodic domain due to perodicity (if it has a displacement of more than maxl*length)                 
                    if ( (abs(xt[p][i+1] - xt[p][i]) > maxl*mesh.lx) or (abs(yt[p][i+1] - yt[p][i]) > maxl*mesh.ly) or (abs(zt[p][i+1] - zt[p][i]) > maxl*mesh.lz) ):
                        #--> initializing its trajectroy history with current re-entry position 
                        xt[p] = [particles.xp[p]]
                        yt[p] = [particles.yp[p]]
                        zt[p] = [particles.zp[p]]
            #plotting the trajectory of particle p
            ax.plot(np.array(xt[p]), np.array(yt[p]), np.array(zt[p]), ':', color ='black', linewidth=1.)
                    
    def it_to_sit(self, it):
        "converts iteration number to appropriate string"
        
        if it < 10:
            sit = "00000"+str(it)
        elif it < 100:
            sit = "0000"+str(it)
        elif it < 1000:
            sit = "000"+str(it)
        elif it < 10000:
            sit = "00"+str(it)
        elif it < 100000:
            sit = "0"+str(it)
        else:
            sit = str(it)
        
        return sit

    def write_mesh(self, mesh, p, t, it):
        "saves the number of particles, equivalent charge and position of all cells in a file for every time step"
                
        particles_per_cell = [len(p.p_in_cells[i,j,k]) for i in range(mesh.nx) for j in range(mesh.ny) for k in range(mesh.nz)]
        cells = np.array([particles_per_cell, mesh.eqQ.flatten(), mesh.eqxp.flatten(), mesh.eqyp.flatten(), mesh.eqzp.flatten()]).transpose()
#        cells = np.array([mesh.pcount.flatten(), mesh.eqQ.flatten(), mesh.eqxp.flatten(), mesh.eqyp.flatten(), mesh.eqzp.flatten()]).transpose()

        sit = self.it_to_sit(it)
        outdir = './mesh/'
        txtname = 'cells_' + str(mesh.ncells) + '_' + self.suffix + '_' + sit + '.txt'
        fmt = ['%4d', '%2.4e', '%2.8f', '%2.8f', '%2.8f']
        h = '\n'.join(['p_per_cell, eqQ, eqxp, eqyp, eqzp', 't = '+str(t)])
        np.savetxt(outdir+txtname, cells, fmt, delimiter='\t', header = h)

    def write_particles(self, p, mesh, t, it):
        "saves the position, radius and charge of all particles in a file for every time step"
        
        fen = np.array([np.linalg.norm(p.fe[i,:]) for i in range(p.npart) ])
        fdn = np.array([np.linalg.norm(p.fd[i,:]) for i in range(p.npart) ])
        distrib = np.array([p.xp, p.yp, p.zp, p.up, p.vp, p.wp, 2*p.r, p.Q, fen, fdn]).transpose() # shape is Nx9
        
        sit = self.it_to_sit(it)  
        outdir = './particles/txt/'
        if p.eforce != 'direct':
            txtname = 'p_' + self.suffix + '_' + str(mesh.nf) + '_' + sit + '.txt'
        else:
            txtname = 'p_' + self.suffix + '_' + sit + '.txt'
        fmt = ['%2.8f', '%2.8f', '%2.8f', '%2.8f', '%2.8f', '%2.8f', '%2.8f', '%2.4e', '%2.4e', '%2.4e']
        h = '\n'.join(['xp, yp, zp, up, vp, wp, dp, q, fe, fd', 't = '+str(t)])
        np.savetxt(outdir+txtname, distrib, fmt, delimiter='\t', header = h)

        #saving the initialization of particles in ../cases for further use
        if p.sdistrib != 'file' and p.sdistrib != 'uniform' and it == 0:          
            initname = 'init_' + str(p.npart) + '_' + ("mono" if p.dispersion == 0 else "poly") + '.txt'
            print 'cp '+ outdir + txtname + ' ../cases/' + initname
            subprocess.call(['cp', outdir + txtname, '../cases/' + initname])

    def write_energy(self, postp, p, t, it):
        "saves the total, kinetic and potential energy of all particles per time step"
        
        energy = np.array([it, t, postp.Ue, postp.Ek, postp.Et, p.E])
        outdir = './post_process/'
        txtname = outdir + 'energy_' + self.suffix + ".txt"
        fmt = ['%d', '%2.6f', '%2.4e', '%2.4e', '%2.4e', '%2.6f']
        h = '#it, t, Ue, Ek, Et, E'
        if it == 0:
            with open(txtname, 'w') as output:
                output.write(h+'\n')   

        with open(txtname, 'ab') as output:         
            np.savetxt(output, np.column_stack(energy), fmt, delimiter='\t')

    def write_correlations(self, postp, p, t, it):
        "saves the total, kinetic and potential energy of all particles per time step"
        
        energy = np.array([it, t, postp.Rp[0,0], postp.Rp[1,1], postp.Rp[2,2], postp.rp, postp.qp2])
        outdir = './post_process/'
        txtname = outdir + 'correl_' + self.suffix + ".txt"
        fmt = ['%d', '%2.6f', '%2.4e', '%2.4e', '%2.4e', '%2.4e', '%2.4e']
        h = '#it, t, Rpii, Rpjj, Rpkk, rp, qp2'
        if it == 0:
            with open(txtname, 'w') as output:
                output.write(h+'\n')    
                
        with open(txtname, 'ab') as output:         
            np.savetxt(output, np.column_stack(energy), fmt, delimiter='\t')

    def printab(self, string):
        "converts iteration number to appropriate string"
    
        tab = "\t"
        string = tab + string
        print string
    
    def unitary_all(self, me):
        "making N arrays (3) contained in a matrix (N,3) unitary"
        
        #if all me is zero (zero velocity for all the particles) then there's no point in calling unitary
        if np.any(me): 
            for i in range(len(me)):
                me[i,:] = particles.unitary(me[i,:])
        else:
            print 'this matrix is all zero'

    def position_plot(self, itfinal, particles, mesh):
        "Create xp(t), yp(t), zp(t) graphs for 8 particles of the domain"
            
        it  = np.arange(0,itfinal,self.it_step)
        it  = np.append(it,itfinal)
        numberoftxt = len(it)
        
        t = np.empty(numberoftxt)
        xp = np.empty((particles.npart,numberoftxt))
        yp = np.empty((particles.npart,numberoftxt))
        zp = np.empty((particles.npart,numberoftxt))
        
        outdir = './particles/txt/'
        if particles.eforce != 'direct':
            txt = 'p_' + self.suffix + '_' + str(mesh.nf)
        else:
            txt = 'p_' + self.suffix
        
        for i in range(len(it)):
        
            sit = self.it_to_sit(it[i])
            txtname = txt + '_' + sit + '.txt'
            with open(outdir+txtname) as f:
                time = f.readlines()[1]
            t[i] = float(time[6:])
            
            xp[:,i], yp[:,i], zp[:,i] = np.genfromtxt(outdir+txtname, usecols = (0,1,2), skip_header=2, unpack=True)

        
        fig, axes = plt.subplots(nrows=3, ncols=1, sharex=True, figsize=(13,11))
        plt.xlabel(r'Time $t$ $(s)$')
        #plt.title('Particles position vs time')
        
        #color=iter(cm.rainbow(np.linspace(0,1,npart)))
        clrs_list=['k','r','b','g'] # list of basic colors
        styl_list=['-','--','-.',':'] # list of basic linestyles
    
        ip = particles.npart / 8
        c = -1
        s = -1
        for p in range(0, particles.npart-ip, ip):
            
            s += 1
            if s % 4 == 0: c += 1
            clrr=clrs_list[c % 4]
            styl=styl_list[s % 4]
        
            axes[0].set_title(r'$x$-position')
            axes[0].set_ylabel(r'$x_p$ $(m)$')
            axes[0].plot(t, xp[p,:], label = 'p '+str(p), c=clrr, ls=styl)
        
            axes[1].set_title(r'$y$-position')  
            axes[1].set_ylabel(r'$y_p$ $(m)$')
            axes[1].plot(t, yp[p,:], label = 'p '+str(p), c=clrr, ls=styl)
        
            axes[2].set_title(r'$z$-position')
            axes[2].set_ylabel(r'$z_p$ $(m)$')
            axes[2].plot(t, zp[p,:], label = 'p '+str(p), c=clrr, ls=styl)
        
        axes[0].legend(loc = 'upper right')
        axes[1].legend(loc = 'upper right')
        axes[2].legend(loc = 'upper right')
        
        mng = plt.get_current_fig_manager()
        ### works on Ubuntu??? >> did NOT working on windows
        mng.resize(*mng.window.maxsize())
        #mng.window.state('zoomed') #works fine on Windows!
        plt.show()
        
        pngname = 'positions_' + self.suffix + '.png'
        plt.savefig(pngname, bbox_inches='tight')
        plt.close(fig)